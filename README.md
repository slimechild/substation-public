# Substation for VCV Rack - Open Source Release

This is the open-source release of Slime Child Audio's Substation for VCV Rack plugin.

This release includes the source code for Substation for VCV Rack, along with some of
its dependencies. It does not include the graphical assets used in the paid version
of the plugin, as they are under copyright. Instead, replacement visual assets are 
provided under the terms of this open-source release's license.

Be aware that Slime Child Audio cannot provide any support or assistance for this open-source release. 
Please purchase the paid version of Substation for VCV Rack if you require product support.
