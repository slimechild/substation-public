/**********
 * Copyright (c) 2022, Coriander Violet Pines. All rights reserved.
 * This file is part of Substation by Slime Child Audio and is governed by the attached license.
 *
 * Author: C. V. Pines
 * FUTURE: eventually, this can be a base class with an internal method that does the json-get/json-sets.
 * FUTURE-FUTURE: ...or a map of polymorphic setting types that intelligently create menu options.
 */

#include "Settings.hpp"

#include <stdio.h>

#include <slime/ui/UI.hpp>

#include <jansson.h>
#include <common.hpp>
#include <logger.hpp>
#include <string.hpp>
#include <system.hpp>
#include <ui/MenuEntry.hpp>
#include <ui/MenuItem.hpp>
#include <ui/MenuLabel.hpp>

namespace slime {
namespace plugin {
namespace substation {

PluginSettings::PluginSettings(void) {
	cable_colors = std::make_shared<std::vector<NVGcolor>>();

	use_custom_cable_colors = std::make_shared<bool>();
	*use_custom_cable_colors = true;
	updateCableColors(*use_custom_cable_colors);
}

PluginSettings::~PluginSettings(void) {
	save();
}

void PluginSettings::appendContextMenu(rack::ui::Menu* menu) {
	menu->addChild(new rack::ui::MenuSeparator);

	rack::ui::MenuLabel* modelLabel = new rack::ui::MenuLabel;
	modelLabel->text = "Plugin Settings";
	menu->addChild(modelLabel);

	using namespace slime::settings;

	// Custom cable colors
	/* REMOVED
	{
		auto item = new slime::ui::BoolSettingMenuItem<>(use_custom_cable_colors);
		item->getInvoker() += std::bind(&PluginSettings::updateCableColors, this, std::placeholders::_1);
		item->text = "Use custom cable colors";
		menu->addChild(item);
	}
	*/
}

void PluginSettings::save() {
	json_t* root = json_object();

	///
	JSON_SET_INT(root, version);
	JSON_SET_BOOL(root, *use_custom_cable_colors);
	///

	rack::system::createDirectory(rack::asset::user(directory));
	std::string path = rack::asset::user(directory + "/" + filename);
	INFO("Saving plugin settings %s", path.c_str());
	FILE* file = fopen(path.c_str(), "w");
	if (!file) {
		WARN("Could save plugin settings %s", path.c_str());
		return;
	}
	DEFER({ fclose(file); });

	json_dumpf(root, file, JSON_INDENT(2) | JSON_REAL_PRECISION(9));
	json_decref(root);
}

void PluginSettings::load() {
	std::string path = rack::asset::user(directory + "/" + filename);
	if (!rack::system::isFile(path)) {
		save();
	}

	INFO("Loading plugin settings %s", path.c_str());

	FILE* file = fopen(path.c_str(), "r");
	if (!file) {
		WARN("Could load plugin settings %s", path.c_str());
		return;
	}
	DEFER({ fclose(file); });

	json_error_t error;
	json_t* root = json_loadf(file, 0, &error);
	if (!root) {
		FATAL("Settings file has invalid JSON at %d:%d %s", error.line, error.column, error.text);
		return;
	}

	// Check version
	JSON_GET_INT(root, version);
	if (version != SETTINGS_VERSION) {
		WARN("Setting version is not supported, and no updator is present. (Your version: %i; Minimum Supported: %i)",
			 version, SETTINGS_VERSION);
		// FUTURE: rewrite the file in this case.
		// FUTURE: handle this when more versions are actually needed.
	}

	///
	JSON_GET_BOOL(root, *use_custom_cable_colors);
	updateCableColors(*use_custom_cable_colors);
	///

	json_decref(root);
}

void PluginSettings::updateCableColors(const bool& value) {
	/* REMOVED */
}

PluginSettings settings;

}  // namespace substation
}  // namespace plugin
}  // namespace slime