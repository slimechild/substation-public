/**********
 * Copyright (c) 2022, Coriander Violet Pines. All rights reserved.
 * This file is part of Substation by Slime Child Audio and is governed by the attached license.
 *
 * Author: C. V. Pines
 */

#include "_plugin.hpp"

#include "Settings.hpp"

rack::plugin::Plugin* pluginInstance;

void init(rack::plugin::Plugin* p) {
	using namespace slime::plugin::substation;

	settings.load();

	pluginInstance = p;

	p->addModel(modelClock);
	p->addModel(modelEnvelopes);
	p->addModel(modelFilter);
	p->addModel(modelMixer);
	p->addModel(modelQuantizer);
	p->addModel(modelPolySequencer);
	p->addModel(modelVCA);
	p->addModel(modelSubOscillator);
	p->addModel(modelBlank4);
	p->addModel(modelBlank7);
	p->addModel(modelFilterPlus);
}
