/**********
 * Copyright (c) 2022, Coriander Violet Pines. All rights reserved.
 * This file is part of Substation by Slime Child Audio and is governed by the attached license.
 *
 * Author: C. V. Pines
 */

#include "_plugin.hpp"

#include "Settings.hpp"

#include <slime/app/ModulePanel.hpp>
#include <slime/skin/Ersatz.hpp>

namespace slime {
namespace plugin {
namespace substation {

struct BlankModule : slime::engine::Module {
	BlankModule(void) { config(0, 0, 0, 0); }

	void process(const ProcessArgs& args) override {}
};

struct Blank4Widget : rack::app::ModuleWidget {
	Blank4Widget(BlankModule* module) {
		setModule(module);

		// Panel
		app::ModulePanel::makePanel(4ull)
			->addImageBackground(rack::asset::plugin(pluginInstance, "res/Blank4_400z.png"))
			->attach(this);

		// Test build warning
#if defined(SLIME_DEBUG)
		skin::ersatz::TestBuildWarning::makeComponent()->attach(this);
#endif

		// Logo
		skin::ersatz::BrandLogo::makeComponent(skin::ersatz::BrandLogo::Size::LOGO)->attach(this);
	}
};

struct Blank7Widget : rack::app::ModuleWidget {
	Blank7Widget(BlankModule* module) {
		setModule(module);

		// Panel
		app::ModulePanel::makePanel(7ull)
			->addImageBackground(rack::asset::plugin(pluginInstance, "res/Blank7_400z.png"))
			->attach(this);

		// Test build warning
#if defined(SLIME_DEBUG)
		skin::ersatz::TestBuildWarning::makeComponent()->attach(this);
#endif

		// Logo
		skin::ersatz::BrandLogo::makeComponent(skin::ersatz::BrandLogo::Size::FULL)->attach(this);
	}
};

rack::plugin::Model* modelBlank4 = rack::createModel<BlankModule, Blank4Widget>("SlimeChild-Substation-OpenSource-Blank-4");
rack::plugin::Model* modelBlank7 = rack::createModel<BlankModule, Blank7Widget>("SlimeChild-Substation-OpenSource-Blank-7");

}  // namespace substation
}  // namespace plugin
}  // namespace slime
