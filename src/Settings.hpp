/**********
 * Copyright (c) 2022, Coriander Violet Pines. All rights reserved.
 * This file is part of Substation by Slime Child Audio and is governed by the attached license.
 *
 * Author: C. V. Pines
 */

#pragma once

#include <memory>
#include <string>

#include <slime/Common.hpp>
#include <slime/IntellisenseFix.hpp>
#include <slime/engine/Settings.hpp>

#include <nanovg.h>
#include <asset.hpp>
#include <ui/Menu.hpp>

namespace slime {
namespace plugin {
namespace substation {

class PluginSettings {
public:
	static const int SETTINGS_VERSION = 1;

	std::string directory = "SlimeChild";
	std::string filename = "Substation.json";

	// Settings
	int version = 1;
	std::shared_ptr<bool> use_custom_cable_colors;  // default set in constructor
	std::shared_ptr<std::vector<NVGcolor>> cable_colors;
	float light_trigger_fade = 0.1f;

	PluginSettings(void);
	~PluginSettings(void);

	void save();
	void load();

	void appendContextMenu(rack::ui::Menu* menu);

private:
	void updateCableColors(const bool& value);
};

extern PluginSettings settings;

}  // namespace substation
}  // namespace plugin
}  // namespace slime
