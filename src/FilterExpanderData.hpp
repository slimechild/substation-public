/**********
 * Copyright (c) 2022, Coriander Violet Pines. All rights reserved.
 * This file is part of Substation by Slime Child Audio and is governed by the attached license.
 *
 * Author: C. V. Pines
 */

#pragma once

#include <array>
#include <cstddef>

#include <slime/Math.hpp>

namespace slime {
namespace plugin {
namespace substation {
namespace detail {

struct FilterToExpanderData {
	std::array<math::float_simd, math::SIMD_PAR> lowpass2;
	std::array<math::float_simd, math::SIMD_PAR> bandpass2;
	std::array<math::float_simd, math::SIMD_PAR> bandpass4;
	std::size_t channels;

	FilterToExpanderData() {
		lowpass2.fill(0.0f);
		bandpass2.fill(0.0f);
		bandpass4.fill(0.0f);
		channels = 0;
	}
};

struct ExpanderToFilterData {
	std::array<math::float_simd, math::SIMD_PAR> resonance_offset;
	bool connected_to_left, connected_to_right;

	ExpanderToFilterData() {
		resonance_offset.fill(0.0f);
		connected_to_left = false;
		connected_to_right = false;
	}
};

}  // namespace detail
}  // namespace substation
}  // namespace plugin
}  // namespace slime
