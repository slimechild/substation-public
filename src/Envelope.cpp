/**********
 * Copyright (c) 2022, Coriander Violet Pines. All rights reserved.
 * This file is part of Substation by Slime Child Audio and is governed by the attached license.
 *
 * Author: C. V. Pines
 */

#include "_plugin.hpp"

#include "Settings.hpp"

#include <cmath>
#include <memory>

#include <slime/app/ModulePanel.hpp>
#include <slime/cv/Digital.hpp>
#include <slime/skin/Ersatz.hpp>

#include <dsp/digital.hpp>
#include <widget/SvgWidget.hpp>

namespace slime {
namespace plugin {
namespace substation {

namespace detail {
struct EnvelopeGenerator {
	enum class Stage {
		IDLE,
		ATTACK,
		DECAY,
	};

	static constexpr float OVERSHOOT = 1.15f;
	static constexpr float COEFF = 2.03688192726f;  // -ln(1 - 1 / 1.15)) causes convergence at t=1
	static constexpr float IDLE_EPS = 0.0f;

	float attack_time = 0.5f;
	float decay_time = 1.0f;
	float value = 0.0f;

	Stage stage = Stage::IDLE;
	float target = 0.0f;
	bool attack_triggered = false;
	bool decay_triggered = false;

	void reset() {
		stage = Stage::IDLE;
		target = 0.0f;
		attack_triggered = false;
		decay_triggered = false;
		value = 0.0f;
	}

	void trigger() {
		if (stage == Stage::ATTACK)
			return;

		target = OVERSHOOT;
		stage = Stage::ATTACK;
		attack_triggered = true;
	}

	float process(float delta_time) {
		if (stage == Stage::IDLE)
			return 0.0f;

		if (stage == Stage::ATTACK) {
			value += COEFF * delta_time * (target - value) / attack_time;

			if (value > 1.0f) {
				value = 1.0f;
				target = 1.0f - OVERSHOOT;
				stage = Stage::DECAY;
				decay_triggered = true;
			}
		} else if (stage == Stage::DECAY) {
			value += COEFF * delta_time * (target - value) / decay_time;

			if (value < IDLE_EPS) {
				value = 0.0f;
				stage = Stage::IDLE;
			}
		}

		return value;
	}

	bool attackWasTriggered(void) {
		bool result = attack_triggered;
		attack_triggered = false;
		return result;
	}

	bool decayWasTriggered(void) {
		bool result = decay_triggered;
		decay_triggered = false;
		return result;
	}
};

}  // namespace detail

struct EnvelopesModule : slime::engine::Module {
	// Mutable config
	float eg1_attack = 1e6f, eg1_decay = 1e6f;
	float eg2_attack = 1e6f, eg2_decay = 1e6f;

	// Internal state
	detail::EnvelopeGenerator eg1, eg2;
	slime::cv::SchmittTrigger trigger1_filter, trigger2_filter;
	rack::dsp::ClockDivider light_divider, param_divider;
	bool hold = false;

	enum ParamIds {
		EG1_ATTACK_PARAM,
		EG1_DECAY_PARAM,
		EG2_ATTACK_PARAM,
		EG2_DECAY_PARAM,
		HOLD_PARAM,
		TRIGGER_PARAM,
		NUM_PARAMS
	};

	enum InputIds { TRIGGER1_INPUT, TRIGGER2_INPUT, NUM_INPUTS };

	enum OutputIds { EG1_OUTPUT, EG2_OUTPUT, NUM_OUTPUTS };

	enum LightIds { EG1_ATTACK_LIGHT, EG1_DECAY_LIGHT, EG2_ATTACK_LIGHT, EG2_DECAY_LIGHT, NUM_LIGHTS };

	EnvelopesModule(void) {
		config(NUM_PARAMS, NUM_INPUTS, NUM_OUTPUTS, NUM_LIGHTS);

		configInput(TRIGGER1_INPUT, "EG1 Trigger");
		configInput(TRIGGER2_INPUT, "EG2 Trigger");
		configOutput(EG1_OUTPUT, "EG1 Envelope");
		configOutput(EG2_OUTPUT, "EG2 Envelope");

		configParam(EG1_ATTACK_PARAM, -3.0f, 1.0f, -1.0f, "EG1 Attack", " ms", 10.0f, 1000.0f);
		configParam(EG1_DECAY_PARAM, -3.0f, 1.0f, -1.0f, "EG1 Decay", " ms", 10.0f, 1000.0f);
		configParam(EG2_ATTACK_PARAM, -3.0f, 1.0f, -1.0f, "EG2 Attack", " ms", 10.0f, 1000.0f);
		configParam(EG2_DECAY_PARAM, -3.0f, 1.0f, -1.0f, "EG2 Decay", " ms", 10.0f, 1000.0f);

		configSwitch(HOLD_PARAM, 0.0f, 1.0f, 0.0f, "Hold", {"OFF", "ON"});
		getParamQuantity(HOLD_PARAM)->randomizeEnabled = false;
		configButton(TRIGGER_PARAM, "Trigger");

		light_divider.setDivision(512);  // Min 86 Hz
		param_divider.setDivision(64);   // Min 689 Hz

		onReset();
	}

	~EnvelopesModule(void) { settings.save(); }

	void onReset() override {
		light_divider.reset();
		param_divider.reset();
		eg1.reset();
		eg2.reset();
	}

	void process(const ProcessArgs& args) override {
		if (param_divider.process()) {
			if (eg1_attack != params[EG1_ATTACK_PARAM].getValue()) {
				eg1_attack = params[EG1_ATTACK_PARAM].getValue();
				// Even though this is costly, it rarely happens, so not worth further optimization
				eg1.attack_time = std::pow(10.0f, eg1_attack);
			}

			if (eg1_decay != params[EG1_DECAY_PARAM].getValue()) {
				eg1_decay = params[EG1_DECAY_PARAM].getValue();
				eg1.decay_time = std::pow(10.0f, eg1_decay);
			}

			if (eg2_attack != params[EG2_ATTACK_PARAM].getValue()) {
				eg2_attack = params[EG2_ATTACK_PARAM].getValue();
				eg2.attack_time = std::pow(10.0f, eg2_attack);
			}

			if (eg2_decay != params[EG2_DECAY_PARAM].getValue()) {
				eg2_decay = params[EG2_DECAY_PARAM].getValue();
				eg2.decay_time = std::pow(10.0f, eg2_decay);
			}

			hold = params[HOLD_PARAM].getValue() > 0.5f;
		}

		trigger1_filter.process(2.0f * params[TRIGGER_PARAM].getValue() + inputs[TRIGGER1_INPUT].getVoltage());
		if (trigger1_filter.isRising()) {
			eg1.trigger();
		}

		trigger2_filter.process(2.0f * params[TRIGGER_PARAM].getValue() + inputs[TRIGGER2_INPUT].getVoltage());
		if (trigger2_filter.isRising()) {
			eg2.trigger();
		}

		eg1.process(args.sampleTime);
		eg2.process(args.sampleTime);

		if (hold) {
			outputs[EG1_OUTPUT].setVoltage(10.0f);
			outputs[EG2_OUTPUT].setVoltage(10.0f);
		} else {
			outputs[EG1_OUTPUT].setVoltage(10.0f * eg1.value);
			outputs[EG2_OUTPUT].setVoltage(10.0f * eg2.value);
		}

		if (light_divider.process()) {
			float downsample = static_cast<float>(light_divider.division);
			lights[EG1_ATTACK_LIGHT].setSmoothBrightness(
				(hold || eg1.attackWasTriggered() || (eg1.stage == detail::EnvelopeGenerator::Stage::ATTACK)) ? 1.0f
																											  : 0.0f,
				args.sampleTime * downsample * settings.light_trigger_fade);
			lights[EG1_DECAY_LIGHT].setSmoothBrightness(
				(hold || eg1.decayWasTriggered() || (eg1.stage == detail::EnvelopeGenerator::Stage::DECAY)) ? 1.0f
																											: 0.0f,
				args.sampleTime * downsample * settings.light_trigger_fade);
			lights[EG2_ATTACK_LIGHT].setSmoothBrightness(
				(hold || eg2.attackWasTriggered() || (eg2.stage == detail::EnvelopeGenerator::Stage::ATTACK)) ? 1.0f
																											  : 0.0f,
				args.sampleTime * downsample * settings.light_trigger_fade);
			lights[EG2_DECAY_LIGHT].setSmoothBrightness(
				(hold || eg2.decayWasTriggered() || (eg2.stage == detail::EnvelopeGenerator::Stage::DECAY)) ? 1.0f
																											: 0.0f,
				args.sampleTime * downsample * settings.light_trigger_fade);
		}
	}
};

struct EnvelopesWidget : rack::app::ModuleWidget {
	EnvelopesWidget(EnvelopesModule* module) {
		setModule(module);

		// Panel
		app::ModulePanel::makePanel(7ull)
			->addImageBackground(rack::asset::plugin(pluginInstance, "res/Envelope_400z.png"))
			->addSvgBackground(rack::asset::plugin(pluginInstance, "res/Envelope_Text.svg"))
			->attach(this);

		using namespace skin::ersatz;

		// Test build warning
#if defined(SLIME_DEBUG)
		TestBuildWarning::makeComponent()->attach(this);
#endif

		// Logo
		BrandLogo::makeComponent(BrandLogo::Size::FULL)->attach(this);

		// Inputs
		rack::math::Vec inputs[] = {{10.16f, 59.70f}, {10.16f, 106.69f}};
		for (size_t i = 0; i < EnvelopesModule::NUM_INPUTS; i++) {
			Port::makeComponent()
				->setPositionMm(inputs[i])
				->center()
				->setInput()
				->setCustomColors(settings.cable_colors)
				->attach(module, i, this);
		}

		// Outputs
		rack::math::Vec outputs[] = {{25.4f, 59.70f}, {25.4f, 106.69f}};
		for (size_t i = 0; i < EnvelopesModule::NUM_OUTPUTS; i++) {
			Port::makeComponent()
				->setPositionMm(outputs[i])
				->center()
				->setOutput()
				->setCustomColors(settings.cable_colors)
				->attach(module, i, this);
		}

		// Knobs
		rack::math::Vec knobs[] = {{10.16f, 40.62f}, {25.4f, 40.62f}, {10.16f, 87.61f}, {25.4f, 87.61f}};
		for (size_t i = 0; i < EnvelopesModule::HOLD_PARAM; i++) {
			Knob::makeComponent(Knob::Size::MEDIUM)->setPositionMm(knobs[i])->center()->attach(module, i, this);
		}

		// Buttons
		Button::makeComponent(Button::Size::SMALL, Button::OffColor::CLEAR, Button::OnColor::GREY)
			->setPositionMm(12.7f, 15.24f)
			->center()
			->setMomentary(false)
			->attach(module, EnvelopesModule::HOLD_PARAM, this);

		Button::makeComponent(Button::Size::SMALL, Button::OffColor::CLEAR, Button::OnColor::GREY)
			->setPositionMm(22.85f, 15.24f)
			->center()
			->setMomentary(true)
			->attach(module, EnvelopesModule::TRIGGER_PARAM, this);

		// Lights
		rack::math::Vec lights[] = {{10.16f, 30.47f}, {25.4f, 30.47f}, {10.16f, 77.46f}, {25.4f, 77.46f}};
		for (size_t i = 0; i < EnvelopesModule::NUM_LIGHTS; i++) {
			MonochromeLED::makeComponent(MonochromeLED::Size::SMALL, MonochromeLED::Color::MINT)
				->setPositionMm(lights[i])
				->center()
				->attach(module, i, this);
		}
	}

	void appendContextMenu(rack::ui::Menu* menu) override { settings.appendContextMenu(menu); }
};

rack::plugin::Model* modelEnvelopes =
	rack::createModel<EnvelopesModule, EnvelopesWidget>("SlimeChild-Substation-OpenSource-Envelopes");

}  // namespace substation
}  // namespace plugin
}  // namespace slime
