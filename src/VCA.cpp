/**********
 * Copyright (c) 2022, Coriander Violet Pines. All rights reserved.
 * This file is part of Substation by Slime Child Audio and is governed by the attached license.
 *
 * Author: C. V. Pines
 */

#include "_plugin.hpp"

#include "Settings.hpp"

#include <cmath>

#include <slime/Math.hpp>
#include <slime/app/ModulePanel.hpp>
#include <slime/skin/Ersatz.hpp>

#include <dsp/digital.hpp>
#include <dsp/filter.hpp>
#include <simd/Vector.hpp>

namespace slime {
namespace plugin {
namespace substation {

struct VCAModule : slime::engine::Module {
	// Internal state
	rack::dsp::PeakFilter level_filter;
	rack::dsp::ClockDivider level_divider, light_divider;

	enum ParamIds { GAIN_PARAM, NUM_PARAMS };

	enum InputIds { GAIN_INPUT, SIGNAL_INPUT, NUM_INPUTS };

	enum OutputIds { SIGNAL_OUTPUT, NUM_OUTPUTS };

	enum LightIds { ENUMS(LEVEL_LIGHT, 3), NUM_LIGHTS };

	VCAModule(void) {
		config(NUM_PARAMS, NUM_INPUTS, NUM_OUTPUTS, NUM_LIGHTS);

		configInput(GAIN_INPUT, "Level");
		configInput(SIGNAL_INPUT, "Signal");
		configOutput(SIGNAL_OUTPUT, "Signal");
		configBypass(SIGNAL_INPUT, SIGNAL_OUTPUT);

		configParam(GAIN_PARAM, 0.0f, 1.0f, 1.0f, "Level", " %", 0.0f, 100.0f);

		light_divider.setDivision(512);

		level_divider.setDivision(8);
		level_filter.setLambda(5.0f);
	}

	void onReset(void) override {
		level_filter.reset();
		level_divider.reset();
		light_divider.reset();
	}

	void process(const ProcessArgs& args) override {
		size_t channels = inputs[SIGNAL_INPUT].getChannels();
		if (channels == 0) {
			outputs[SIGNAL_OUTPUT].setChannels(1);
			outputs[SIGNAL_OUTPUT].setVoltage(0.0f);

			if (level_divider.process()) {
				level_filter.process(args.sampleTime * level_divider.division, 0.0f);
			}
		}

		float gain = params[GAIN_PARAM].getValue();
		gain *= gain;

		if (inputs[GAIN_INPUT].isConnected()) {
			gain *= std::max(inputs[GAIN_INPUT].getVoltage() * 0.1f, 0.0f);
		}

		outputs[SIGNAL_OUTPUT].setChannels(channels);
		for (size_t ch = 0; ch < channels; ch += math::float_simd::size) {
			math::float_simd in = inputs[SIGNAL_INPUT].getVoltageSimd<math::float_simd>(ch);
			math::float_simd out = gain * in;
			outputs[SIGNAL_OUTPUT].setVoltageSimd(out, ch);

			if ((ch == 0) && level_divider.process()) {
				level_filter.process(args.sampleTime * static_cast<float>(level_divider.division), std::abs(out[0]));
			}
		}

		if (light_divider.process()) {
			float val = level_filter.out * 0.4f;
			lights[LEVEL_LIGHT].setBrightness(val);
			lights[LEVEL_LIGHT + 1].setBrightness(val - 1.0f);
			lights[LEVEL_LIGHT + 2].setBrightness(val - 2.0f);
		}
	}
};

struct VCAWidget : rack::app::ModuleWidget {
	VCAWidget(VCAModule* module) {
		setModule(module);

		// Panel
		app::ModulePanel::makePanel(4ull)
			->addImageBackground(rack::asset::plugin(pluginInstance, "res/VCA_400z.png"))
			->addSvgBackground(rack::asset::plugin(pluginInstance, "res/VCA_Text.svg"))
			->attach(this);

		using namespace skin::ersatz;

		// Test build warning
#if defined(SLIME_DEBUG)
		TestBuildWarning::makeComponent()->attach(this);
#endif

		// Logo
		BrandLogo::makeComponent(BrandLogo::Size::LOGO)->attach(this);

		// Inputs
		rack::math::Vec inputs[] = {{0.4f, 2.35f}, {0.4f, 3.55f}};
		for (size_t i = 0; i < VCAModule::NUM_INPUTS; i++) {
			Port::makeComponent()
				->setPositionIn(inputs[i])
				->center()
				->setInput()
				->setCustomColors(settings.cable_colors)
				->attach(module, i, this);
		}

		// Outputs
		Port::makeComponent()
			->setPositionIn(0.4f, 4.2f)
			->center()
			->setOutput()
			->setCustomColors(settings.cable_colors)
			->attach(module, VCAModule::SIGNAL_OUTPUT, this);

		// Knobs
		Knob::makeComponent(Knob::Size::MEDIUM)
			->setPositionIn(0.4f, 0.8f)
			->center()
			->attach(module, VCAModule::GAIN_PARAM, this);

		// Lights
		rack::math::Vec lights[] = {{0.2f, 3.05f}, {0.4f, 3.05f}, {0.6f, 3.05f}};
		for (size_t i = 0; i < VCAModule::NUM_LIGHTS; i++) {
			MonochromeLED::makeComponent(MonochromeLED::Size::SMALL, MonochromeLED::Color::MINT)
				->setPositionIn(lights[i])
				->center()
				->attach(module, i, this);
		}
	}

	void appendContextMenu(rack::ui::Menu* menu) override { settings.appendContextMenu(menu); }
};

rack::plugin::Model* modelVCA = rack::createModel<VCAModule, VCAWidget>("SlimeChild-Substation-OpenSource-VCA");

}  // namespace substation
}  // namespace plugin
}  // namespace slime
