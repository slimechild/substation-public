/**********
 * Copyright (c) 2022, Coriander Violet Pines. All rights reserved.
 * This file is part of Substation by Slime Child Audio and is governed by the attached license.
 *
 * Author: C. V. Pines
 */

#include "_plugin.hpp"

#include "Settings.hpp"

#include <cmath>

#include <slime/Math.hpp>
#include <slime/app/ModulePanel.hpp>
#include <slime/cv/Digital.hpp>
#include <slime/dsp/ClockGenerator.hpp>
#include <slime/skin/Ersatz.hpp>

#include <common.hpp>
#include <dsp/approx.hpp>
#include <dsp/digital.hpp>

namespace slime {
namespace plugin {
namespace substation {

struct ClockModule : slime::engine::Module {
	// Internal state
	dsp::ClockGenerator base_clock, mult_clock;
	cv::SchmittTrigger run_trigger, sync_trigger;
	rack::dsp::ClockDivider param_divider;
	bool running;
	bool sync;

	enum ParamIds { FREQ_PARAM, RUN_PARAM, MULT_PARAM, NUM_PARAMS };

	enum InputIds { RUN_INPUT, SYNC_INPUT, NUM_INPUTS };

	enum OutputIds { BASE_OUTPUT, MULT_OUTPUT, NUM_OUTPUTS };

	enum LightIds { NUM_LIGHTS };

	ClockModule(void) {
		config(NUM_PARAMS, NUM_INPUTS, NUM_OUTPUTS, NUM_LIGHTS);

		onReset();

		configInput(RUN_INPUT, "Run enable");
		configInput(SYNC_INPUT, "Sync");
		configOutput(BASE_OUTPUT, "Base clock");
		configOutput(MULT_OUTPUT, "Multiplied clock");

		// 1/3 Hz -> 12 Hz, or 20 BPM to 720 BPM, with up to 16x multiplication
		configParam(FREQ_PARAM, -std::log2f(3.0f), std::log2f(12.0f), 1.0f, "Tempo", " BPM", 2.0f, 60.0f);
		configSwitch(RUN_PARAM, 0.0f, 1.0f, 0.0f, "Run", {"OFF", "ON"});
		getParamQuantity(RUN_PARAM)->randomizeEnabled = false;
		configParam(MULT_PARAM, 1.0f, 16.0f, 1.0f, "Multiplier");

		param_divider.setDivision(64);  // Min 689 Hz
	}

	void onReset(void) override {
		base_clock.reset();
		mult_clock.reset();
		run_trigger.reset();
		sync_trigger.reset();
		param_divider.reset();
		running = false;
		sync = false;
	}

	void toggleRun() {
		running = !running;
		params[RUN_PARAM].setValue(running ? 1.0f : 0.0f);
	}

	void process(const ProcessArgs& args) override {
		if (param_divider.process()) {
			running = params[RUN_PARAM].getValue() > 0.5f;

			if (inputs[RUN_INPUT].isConnected()) {
				run_trigger.process(inputs[RUN_INPUT].getVoltage());

				if (run_trigger.isRising()) {
					running = true;
					params[RUN_PARAM].setValue(1.0f);
				}

				if (run_trigger.isFalling()) {
					running = false;
					params[RUN_PARAM].setValue(0.0f);
				}
			}

			sync_trigger.process(inputs[SYNC_INPUT].getVoltage());
			if (sync_trigger.isRising()) {
				sync = true;
			}

			float freq = rack::dsp::approxExp2_taylor5(params[FREQ_PARAM].getValue() + 2.0f) * 0.25f;
			base_clock.setFrequency(freq);
			mult_clock.setFrequency(params[MULT_PARAM].getValue() * freq);
		}

		if (sync) {
			sync = false;
			base_clock.sync();
			mult_clock.sync();
		}

		if (running) {
			float last_base = base_clock.value;

			base_clock.process(args.sampleTime);

			if ((base_clock.value > 0.5f) && (last_base < 0.5f)) {
				mult_clock.sync();
			}

			mult_clock.process(args.sampleTime);
		}

		outputs[BASE_OUTPUT].setVoltage(base_clock.value * 10.0f);
		outputs[MULT_OUTPUT].setVoltage(mult_clock.value * 10.0f);
	};
};

struct ClockWidget : rack::app::ModuleWidget {
	ClockWidget(ClockModule* module) {
		setModule(module);

		// Panel
		app::ModulePanel::makePanel(7)
			->addImageBackground(rack::asset::plugin(pluginInstance, "res/Clock_400z.png"))
			->addSvgBackground(rack::asset::plugin(pluginInstance, "res/Clock_Text.svg"))
			->attach(this);

		using namespace skin::ersatz;

		// Test build warning
#if defined(SLIME_DEBUG)
		TestBuildWarning::makeComponent()->attach(this);
#endif

		// Logo
		BrandLogo::makeComponent(BrandLogo::Size::FULL)->attach(this);

		// Inputs
		rack::math::Vec inputs[] = {{0.4f, 3.55f}, {0.4f, 4.2f}};
		for (size_t i = 0; i < ClockModule::NUM_INPUTS; i++) {
			Port::makeComponent()
				->setPositionIn(inputs[i])
				->center()
				->setInput()
				->setCustomColors(settings.cable_colors)
				->attach(module, i, this);
		}

		// Outputs
		rack::math::Vec outputs[] = {{1.0f, 3.55f}, {1.0f, 4.2f}};
		for (size_t i = 0; i < ClockModule::NUM_OUTPUTS; i++) {
			Port::makeComponent()
				->setPositionIn(outputs[i])
				->center()
				->setOutput()
				->setCustomColors(settings.cable_colors)
				->attach(module, i, this);
		}

		// Buttons
		Button::makeComponent(Button::Size::MEDIUM, Button::OffColor::CLEAR, Button::OnColor::GREY)
			->setPositionIn(0.4f, 2.15f)
			->center()
			->setMomentary(false)
			->attach(module, ClockModule::RUN_PARAM, this);

		// Knobs
		Knob::makeComponent(Knob::Size::BIG)
			->setPositionIn(0.7f, 1.1f)
			->center()
			->attach(module, ClockModule::FREQ_PARAM, this);

		Knob::makeComponent(Knob::Size::MEDIUM)
			->setPositionIn(1.0f, 2.15f)
			->center()
			->setSnapping(true)
			->attach(module, ClockModule::MULT_PARAM, this);
	}

	void appendContextMenu(rack::ui::Menu* menu) override { settings.appendContextMenu(menu); }

	void onHoverKey(const rack::event::HoverKey& e) override {
		if (e.action == GLFW_PRESS) {
			if (e.key == GLFW_KEY_SPACE && ((e.mods & RACK_MOD_MASK) == 0)) {
				auto* pmodule = dynamic_cast<ClockModule*>(this->module);
				if (pmodule) {
					pmodule->toggleRun();
				}

				e.consume(this);
				return;
			}
		}

		ModuleWidget::onHoverKey(e);
	}
};

rack::plugin::Model* modelClock = rack::createModel<ClockModule, ClockWidget>("SlimeChild-Substation-OpenSource-Clock");

}  // namespace substation
}  // namespace plugin
}  // namespace slime
