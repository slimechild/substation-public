/**********
 * Copyright (c) 2022, Coriander Violet Pines. All rights reserved.
 * This file is part of Substation by Slime Child Audio and is governed by the attached license.
 *
 * Author: C. V. Pines
 */

#include "_plugin.hpp"

#include "FilterExpanderData.hpp"
#include "Settings.hpp"

#include <slime/Math.hpp>
#include <slime/app/ModulePanel.hpp>
#include <slime/engine/Expanders.hpp>
#include <slime/skin/Ersatz.hpp>

#include <dsp/digital.hpp>
#include <simd/Vector.hpp>

namespace slime {
namespace plugin {
namespace substation {

typedef engine::ExpandableModule<detail::ExpanderToFilterData, detail::FilterToExpanderData> Base;

struct FilterPlusModule : Base {
	// Internal state
	rack::dsp::ClockDivider light_divider;
	detail::ExpanderToFilterData* outgoing_data;
	detail::FilterToExpanderData* incoming_data;
	bool use_left, use_right;

	enum ParamIds { RES_MOD_PARAM, NUM_PARAMS };

	enum InputIds { RES_CV_INPUT, NUM_INPUTS };

	enum OutputIds { LP2_OUTPUT, BP2_OUTPUT, BP4_OUTPUT, NUM_OUTPUTS };

	enum LightIds { LEFT_EXPANDER_LIGHT, RIGHT_EXPANDER_LIGHT, NUM_LIGHTS };

	FilterPlusModule() : Base() {
		config(NUM_PARAMS, NUM_INPUTS, NUM_OUTPUTS, NUM_LIGHTS);

		configInput(RES_CV_INPUT, "Resonance");
		configOutput(LP2_OUTPUT, "2-Pole lowpass");
		configOutput(BP2_OUTPUT, "2-Pole bandpass");
		configOutput(BP4_OUTPUT, "4-Pole bandpass");

		configParam(RES_MOD_PARAM, -1.0f, 1.0f, 0.0f, "Resonance Mod Amount", "%", 0.0f, 100.0f);

		light_divider.setDivision(512);

		incoming_data = nullptr;
		outgoing_data = nullptr;
		use_left = false;
		use_right = false;
	}

	void onReset() override {}

	void process(const ProcessArgs& args) override {
		if (updateExpanders()) {
			// Prefer left
			use_left = hasLeftExpander();
			use_right = (!use_left) && hasRightExpander();
		}

		// Select the module to connect to
		if (use_left) {
			incoming_data = fromLeftExpander();
			outgoing_data = toLeftExpander();
		} else if (use_right) {
			incoming_data = fromRightExpander();
			outgoing_data = toRightExpander();
		} else {
			incoming_data = nullptr;
			outgoing_data = nullptr;
		}

		toLeftExpander()->connected_to_left = use_left;
		toLeftExpander()->connected_to_right = use_right;
		toRightExpander()->connected_to_left = use_left;
		toRightExpander()->connected_to_right = use_right;

		// Incoming message
		if (incoming_data != nullptr) {
			size_t in_channels = incoming_data->channels;
			outputs[LP2_OUTPUT].setChannels(in_channels);
			outputs[BP2_OUTPUT].setChannels(in_channels);
			outputs[BP4_OUTPUT].setChannels(in_channels);
			for (size_t ch = 0; ch < in_channels; ch += math::float_simd::size) {
				size_t simd_index = ch / math::float_simd::size;

				outputs[LP2_OUTPUT].setVoltageSimd(incoming_data->lowpass2[simd_index], ch);
				outputs[BP2_OUTPUT].setVoltageSimd(incoming_data->bandpass2[simd_index], ch);
				outputs[BP4_OUTPUT].setVoltageSimd(incoming_data->bandpass4[simd_index], ch);
			}
		} else {
			// No connection
			outputs[LP2_OUTPUT].setChannels(1);
			outputs[BP2_OUTPUT].setChannels(1);
			outputs[BP4_OUTPUT].setChannels(1);
			outputs[LP2_OUTPUT].setVoltage(0.0f);
			outputs[BP2_OUTPUT].setVoltage(0.0f);
			outputs[BP4_OUTPUT].setVoltage(0.0f);
		}

		// Outgoing message
		float res_scale = params[RES_MOD_PARAM].getValue() * 0.12f;  // 10 volts -> 120%
		if (outgoing_data != nullptr) {
			for (size_t ch = 0; ch < rack::engine::PORT_MAX_CHANNELS; ch += math::float_simd::size) {
				size_t simd_index = ch / math::float_simd::size;

				outgoing_data->resonance_offset[simd_index] =
					res_scale * inputs[RES_CV_INPUT].getPolyVoltageSimd<math::float_simd>(ch);
			}
		}

		// Lights
		if (light_divider.process()) {
			lights[LEFT_EXPANDER_LIGHT].setSmoothBrightness(
				use_left ? 1.0f : 0.0f, args.sampleTime * light_divider.division * settings.light_trigger_fade);
			lights[RIGHT_EXPANDER_LIGHT].setSmoothBrightness(
				use_right ? 1.0f : 0.0f, args.sampleTime * light_divider.division * settings.light_trigger_fade);
		}

		sendExpanderMessages();
	}

	inline bool matchLeftSlug(const std::string& value) override { return value == modelFilter->slug; }
	inline bool matchRightSlug(const std::string& value) override { return value == modelFilter->slug; }
};

struct FilterPlusWidget : rack::app::ModuleWidget {
	FilterPlusWidget(FilterPlusModule* module) {
		setModule(module);

		// Panel
		app::ModulePanel::makePanel(4ull)
			->addImageBackground(rack::asset::plugin(pluginInstance, "res/FilterPlus_400z.png"))
			->addSvgBackground(rack::asset::plugin(pluginInstance, "res/FilterPlus_Text.svg"))
			->attach(this);

		using namespace skin::ersatz;

		// Test build warning
#if defined(SLIME_DEBUG)
		TestBuildWarning::makeComponent()->attach(this);
#endif

		// Logo
		BrandLogo::makeComponent(BrandLogo::Size::LOGO)->attach(this);

		// Inputs
		Port::makeComponent()
			->setPositionIn(0.4f, 1.7f)
			->center()
			->setInput()
			->setCustomColors(settings.cable_colors)
			->attach(module, FilterPlusModule::RES_CV_INPUT, this);

		// Outputs
		rack::math::Vec outputs[] = {{0.4f, 2.35f}, {0.4f, 3.55f}, {0.4f, 4.2f}};
		for (size_t i = 0; i < FilterPlusModule::NUM_OUTPUTS; i++) {
			Port::makeComponent()
				->setPositionIn(outputs[i])
				->center()
				->setOutput()
				->setCustomColors(settings.cable_colors)
				->attach(module, i, this);
		}

		// Knobs
		Knob::makeComponent(Knob::Size::MEDIUM)
			->setPositionIn(0.4f, 0.8f)
			->center()
			->attach(module, FilterPlusModule::RES_MOD_PARAM, this);

		// Lights
		rack::math::Vec lights[] = {{0.2f, 3.05f}, {0.6f, 3.05f}};
		for (size_t i = 0; i < FilterPlusModule::NUM_LIGHTS; i++) {
			MonochromeLED::makeComponent(MonochromeLED::Size::SMALL, MonochromeLED::Color::MINT)
				->setPositionIn(lights[i])
				->center()
				->attach(module, i, this);
		}
	}

	void appendContextMenu(rack::ui::Menu* menu) override { settings.appendContextMenu(menu); }
};

rack::plugin::Model* modelFilterPlus =
	rack::createModel<FilterPlusModule, FilterPlusWidget>("SlimeChild-Substation-OpenSource-Filter-Expander");

}  // namespace substation
}  // namespace plugin
}  // namespace slime