/**********
 * Copyright (c) 2022, Coriander Violet Pines. All rights reserved.
 * This file is part of Substation by Slime Child Audio and is governed by the attached license.
 *
 * Author: C. V. Pines
 */

#include "_plugin.hpp"

#include "Settings.hpp"

#include <slime/app/ModulePanel.hpp>
#include <slime/cv/Quantizer.hpp>
#include <slime/engine/ParamChangeHelper.hpp>
#include <slime/skin/Ersatz.hpp>
#include <slime/ui/UI.hpp>

#include <dsp/digital.hpp>
#include <math.hpp>

namespace slime {
namespace plugin {
namespace substation {

struct QuantizerModule : slime::engine::Module {
	enum Temperment { JUST, EQUAL };
	enum Degrees { EIGHT, TWELVE };

	// Mutable config
	std::shared_ptr<bool> use_minor_scale;  // JSON serialized

	// Internal state
	slime::cv::Quantizer quantizer;
	slime::engine::ParamChangeHelper param_helper;
	rack::dsp::ClockDivider light_divider, process_divider;

	enum ParamIds { TEMPERAMENT_PARAM, DEGREES_PARAM, ROOT_PARAM, OCTAVE_PARAM, TRANSPOSE_PARAM, NUM_PARAMS };

	enum InputIds { ROOT_INPUT, OCTAVE_INPUT, V_OCT_INPUT, NUM_INPUTS };

	enum OutputIds { V_OCT_OUTPUT, NUM_OUTPUTS };

	enum LightIds { DOWN_LIGHT, UP_LIGHT, NUM_LIGHTS };

	QuantizerModule(void) : param_helper(this) {
		use_minor_scale = std::make_shared<bool>(false);

		config(NUM_PARAMS, NUM_INPUTS, NUM_OUTPUTS, NUM_LIGHTS);

		configInput(ROOT_INPUT, "Scale root");
		configInput(OCTAVE_INPUT, "Octave transposition");
		configInput(V_OCT_INPUT, "V/Oct signal");
		configOutput(V_OCT_OUTPUT, "V/Oct signal");
		configBypass(V_OCT_INPUT, V_OCT_OUTPUT);

		configParam(OCTAVE_PARAM, -4.0f, 4.0f, 0.0f, "Octave");
		configSwitch(TEMPERAMENT_PARAM, 0.0f, 1.0f, 0.0f, "Temperament", {"JUST", "EQUAL"});
		configSwitch(DEGREES_PARAM, 0.0f, 1.0f, 0.0f, "Scale Degrees", {"DIATONIC", "CHROMATIC"});
		configSwitch(ROOT_PARAM, 0.0f, 11.0f, 0.0f, "Root Note",
					 {"C", "C#/Db", "D", "D#/Eb", "E", "F", "F#/Gb", "G", "G#/Ab", "A", "A#/Bb", "B"});
		configSwitch(TRANSPOSE_PARAM, 0.0f, 1.0f, 0.0f, "Transpose", {"OFF", "ON"});

		process_divider.setDivision(64);  // Min 689 Hz
		light_divider.setDivision(8);     // Inside process_divider; Min 86 Hz
		onReset();
	}

	void onReset() override {
		param_helper.reset();
		light_divider.reset();
		process_divider.reset();
	}

	void process(const ProcessArgs& args) override {
		if (!process_divider.process()) {
			return;
		}

		if (param_helper.changed(TEMPERAMENT_PARAM) || param_helper.changed(DEGREES_PARAM)) {
			updateScale(static_cast<Temperment>(param_helper.value(TEMPERAMENT_PARAM)),
						static_cast<Degrees>(param_helper.value(DEGREES_PARAM)));
		}

		float root = inputs[ROOT_INPUT].getVoltage();
		root -= std::floor(root);
		root = rack::math::clamp((1.0f / 12.0f) * std::round(params[ROOT_PARAM].getValue()) + root, 0.0f, 1.0f);

		float oct_add = std::floor(inputs[OCTAVE_INPUT].getVoltage() + params[OCTAVE_PARAM].getValue());

		size_t channels = inputs[V_OCT_INPUT].getChannels();
		channels = channels < 1 ? 1 : channels;
		outputs[V_OCT_OUTPUT].setChannels(channels);
		for (size_t ch = 0; ch < channels; ch++) {
			float in = inputs[V_OCT_INPUT].getVoltage(ch);

			if (params[TRANSPOSE_PARAM].getValue() > 0.5f) {
				in += root;
			}

			// Minor scale is just a transposition of the root
			float out = quantizer.process(in, (*use_minor_scale) ? root + 0.25f : root);

			if ((ch == 0) && light_divider.process()) {
				float undersample = static_cast<float>(light_divider.division * process_divider.division);
				lights[DOWN_LIGHT].setSmoothBrightness((out < in - 0.0008f) ? 1.0f : 0.0f,
													   args.sampleTime * undersample * settings.light_trigger_fade);
				lights[UP_LIGHT].setSmoothBrightness((out > in + 0.0008f) ? 1.0f : 0.0f,
													 args.sampleTime * undersample * settings.light_trigger_fade);
			}

			outputs[V_OCT_OUTPUT].setVoltage(out + oct_add, ch);
		}
	}

	void updateScale(Temperment temperament, Degrees degrees) {
		if (temperament == Temperment::JUST) {
			if (degrees == Degrees::EIGHT) {
				quantizer.setScale(slime::cv::Quantizer::DIATONIC_SCALE_JI);
			} else {
				quantizer.setScale(slime::cv::Quantizer::CHROMATIC_SCALE_JI);
			}
		} else {
			if (degrees == Degrees::EIGHT) {
				quantizer.setScale(slime::cv::Quantizer::DIATONIC_SCALE_ET);
			} else {
				quantizer.setScale(slime::cv::Quantizer::CHROMATIC_SCALE_ET);
			}
		}
	}

	json_t* dataToJson() override {
		json_t* root = json_object();

		// Use minor scale
		json_object_set_new(root, "use_minor_scale", json_boolean(*use_minor_scale));

		return root;
	}

	void dataFromJson(json_t* root) override {
		// Use minor scale
		json_t* use_minor_scale_json = json_object_get(root, "use_minor_scale");
		if (use_minor_scale_json) {
			*use_minor_scale = json_boolean_value(use_minor_scale_json);
		}
	}
};

struct QuantizerWidget : rack::app::ModuleWidget {
	QuantizerWidget(QuantizerModule* module) {
		setModule(module);

		// Panel
		app::ModulePanel::makePanel(7ull)
			->addImageBackground(rack::asset::plugin(pluginInstance, "res/Quantizer_400z.png"))
			->addSvgBackground(rack::asset::plugin(pluginInstance, "res/Quantizer_Text.svg"))
			->attach(this);

		using namespace skin::ersatz;

		// Test build warning
#if defined(SLIME_DEBUG)
		TestBuildWarning::makeComponent()->attach(this);
#endif

		// Logo
		BrandLogo::makeComponent(BrandLogo::Size::FULL)->attach(this);

		// Inputs
		rack::math::Vec inputs[] = {{0.4f, 3.55f}, {1.0f, 3.55f}, {0.4f, 4.2f}};
		for (size_t i = 0; i < QuantizerModule::NUM_INPUTS; i++) {
			Port::makeComponent()
				->setPositionIn(inputs[i])
				->center()
				->setInput()
				->setCustomColors(settings.cable_colors)
				->attach(module, i, this);
		}

		// Outputs
		Port::makeComponent()
			->setPositionIn(1.0f, 4.2f)
			->center()
			->setOutput()
			->setCustomColors(settings.cable_colors)
			->attach(module, QuantizerModule::V_OCT_OUTPUT, this);

		// Switches
		SlideSwitch::makeComponent(SlideSwitch::Orientation::HORIZONTAL, SlideSwitch::Positions::DOUBLE_THROW)
			->setPositionIn(0.7f, 0.6f)
			->center()
			->setMomentary(false)
			->attach(module, QuantizerModule::TEMPERAMENT_PARAM, this);

		SlideSwitch::makeComponent(SlideSwitch::Orientation::HORIZONTAL, SlideSwitch::Positions::DOUBLE_THROW)
			->setPositionIn(0.7f, 0.8f)
			->center()
			->setMomentary(false)
			->attach(module, QuantizerModule::DEGREES_PARAM, this);

		// Knobs
		Knob::makeComponent(Knob::Size::MEDIUM)
			->setPositionIn(0.7f, 1.6f)
			->center()
			->setSnapping(true)
			->attach(module, QuantizerModule::ROOT_PARAM, this);

		Knob::makeComponent(Knob::Size::MEDIUM)
			->setPositionIn(1.0f, 2.4f)
			->center()
			->setSnapping(true)
			->attach(module, QuantizerModule::OCTAVE_PARAM, this);

		// Button
		Button::makeComponent(Button::Size::MEDIUM, Button::OffColor::CLEAR, Button::OnColor::GREY)
			->setPositionIn(0.4f, 2.4f)
			->center()
			->setMomentary(false)
			->attach(module, QuantizerModule::TRANSPOSE_PARAM, this);

		// Lights
		MonochromeLED::makeComponent(MonochromeLED::Size::SMALL, MonochromeLED::Color::MINT)
			->setPositionIn(0.4f, 3.05f)
			->center()
			->attach(module, QuantizerModule::DOWN_LIGHT, this);

		MonochromeLED::makeComponent(MonochromeLED::Size::SMALL, MonochromeLED::Color::MINT)
			->setPositionIn(1.0f, 3.05f)
			->center()
			->attach(module, QuantizerModule::UP_LIGHT, this);
	}

	void appendContextMenu(rack::ui::Menu* menu) override {
		auto* pmodule = dynamic_cast<QuantizerModule*>(this->module);
		if (!pmodule) {
			FATAL("Module was not set when appending context menu; cannot bind callbacks.");
			return;
		}

		menu->addChild(new rack::ui::MenuSeparator);

		rack::ui::MenuLabel* modelLabel = new rack::ui::MenuLabel;
		modelLabel->text = "Module Settings";
		menu->addChild(modelLabel);

		{  // Use minor scale
			auto minor = new slime::ui::MapChoiceMenuItem<bool>(pmodule->use_minor_scale,
																{{false, "Major"}, {true, "Natural Minor"}});
			minor->text = "Scale";
			menu->addChild(minor);
		}

		settings.appendContextMenu(menu);
	}
};

rack::plugin::Model* modelQuantizer =
	rack::createModel<QuantizerModule, QuantizerWidget>("SlimeChild-Substation-OpenSource-Quantizer");

}  // namespace substation
}  // namespace plugin
}  // namespace slime
