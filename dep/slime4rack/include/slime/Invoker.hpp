/**********
 * Copyright (c) 2022, Coriander Violet Pines. All rights reserved.
 * This file is part of slime4rack and is governed by the attached license.
 *
 * Author: C. V. Pines
 * Status: un-stable
 * Compatibility: 2.0.0
 */

#pragma once

#include <functional>
#include <vector>

namespace slime {

// 3 arguments
template <typename T = void, typename U = void, typename V = void>
class Invoker {
public:
	typedef std::function<void(const T&, const U&, const V&)> CallbackType;

	Invoker() : _callbacks() {}
	Invoker(const Invoker<T, U, V>&) = delete;
	Invoker(Invoker<T, U, V>&&) noexcept = delete;
	Invoker<T, U, V>& operator=(const Invoker<T, U, V>&) = delete;
	Invoker<T, U, V>* operator=(Invoker<T, U, V>&&) noexcept = delete;

	void registerCallback(const CallbackType& callback) { _callbacks.push_back(callback); }

	void invoke(const T& v1, const U& v2, const V& v3) {
		for (const CallbackType& func : _callbacks) {
			func(v1, v2, v3);
		}
	}

	Invoker& operator+=(const CallbackType& callback) {
		registerCallback(callback);
		return *this;
	}

private:
	std::vector<CallbackType> _callbacks;
};

// 2 arguments
template <typename T, typename U>
class Invoker<T, U, void> {
public:
	typedef std::function<void(const T&, const U&)> CallbackType;

	Invoker() : _callbacks() {}
	Invoker(const Invoker<T, U>&) = delete;
	Invoker(Invoker<T, U>&&) noexcept = delete;
	Invoker<T, U>& operator=(const Invoker<T, U>&) = delete;
	Invoker<T, U>* operator=(Invoker<T, U>&&) noexcept = delete;

	void registerCallback(const CallbackType& callback) { _callbacks.push_back(callback); }

	void invoke(const T& v1, const U& v2) {
		for (const CallbackType& func : _callbacks) {
			func(v1, v2);
		}
	}

	Invoker& operator+=(const CallbackType& callback) {
		registerCallback(callback);
		return *this;
	}

private:
	std::vector<CallbackType> _callbacks;
};

// 1 argument
template <typename T>
class Invoker<T, void> {
public:
	typedef std::function<void(const T&)> CallbackType;

	Invoker() : _callbacks() {}
	Invoker(const Invoker<T>&) = delete;
	Invoker(Invoker<T>&&) noexcept = delete;
	Invoker<T>& operator=(const Invoker<T>&) = delete;
	Invoker<T>* operator=(Invoker<T>&&) noexcept = delete;

	void registerCallback(const CallbackType& callback) { _callbacks.push_back(callback); }

	void invoke(const T& value) {
		for (const CallbackType& func : _callbacks) {
			func(value);
		}
	}

	Invoker& operator+=(const CallbackType& callback) {
		registerCallback(callback);
		return *this;
	}

private:
	std::vector<CallbackType> _callbacks;
};

// void
template <>
class Invoker<void> {
public:
	typedef std::function<void()> CallbackType;

	Invoker() : _callbacks() {}
	Invoker(const Invoker<>&) = delete;
	Invoker(Invoker<>&&) noexcept = delete;
	Invoker<>& operator=(const Invoker<>&) = delete;
	Invoker<>* operator=(Invoker<>&&) noexcept = delete;

	void registerCallback(const CallbackType& callback) { _callbacks.push_back(callback); }

	void invoke() {
		for (const CallbackType& func : _callbacks) {
			func();
		}
	}

	Invoker& operator+=(const CallbackType& callback) {
		registerCallback(callback);
		return *this;
	}

private:
	std::vector<CallbackType> _callbacks;
};

}  // namespace slime