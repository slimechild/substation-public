/**********
 * Copyright (c) 2022, Coriander Violet Pines. All rights reserved.
 * This file is part of slime4rack and is governed by the attached license.
 *
 * Author: C. V. Pines
 * Status: stable
 * Compatibility: 1.2.0
 */

#pragma once

#include <cstddef>
#include <memory>
#include <string>
#include <type_traits>
#include <utility>

#include "../dep/span.hpp"  // Implement std::span since it's not in C++11

#define SLIME_STRINGIFY(x) #x

namespace slime {

// Import span, size into namespace
using tcb::make_span;
using tcb::span;
using tcb::detail::size;

// Implement std::make_unique since it's not in C++11
namespace detail {
template <class T>
struct _Unique_if {
	typedef std::unique_ptr<T> _Single_object;
};

template <class T>
struct _Unique_if<T[]> {
	typedef std::unique_ptr<T[]> _Unknown_bound;
};

template <class T, size_t N>
struct _Unique_if<T[N]> {
	typedef void _Known_bound;
};
}  // namespace detail

template <class T, class... Args>
typename detail::_Unique_if<T>::_Single_object make_unique(Args&&... args) {
	return std::unique_ptr<T>(new T(std::forward<Args>(args)...));
}

template <class T>
typename detail::_Unique_if<T>::_Unknown_bound make_unique(size_t n) {
	typedef typename std::remove_extent<T>::type U;
	return std::unique_ptr<T>(new U[n]());
}

template <class T, class... Args>
typename detail::_Unique_if<T>::_Known_bound make_unique(Args&&...) = delete;

}  // namespace slime
