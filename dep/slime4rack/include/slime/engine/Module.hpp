/**********
 * Copyright (c) 2022, Coriander Violet Pines. All rights reserved.
 * This file is part of slime4rack and is governed by the attached license.
 *
 * Author: C. V. Pines
 * Status: stable
 * Compatibility: v1.1.0
 */

#pragma once

#include <cstddef>
#include <map>

#include <engine/Module.hpp>

namespace slime {
namespace engine {

struct Module : public rack::engine::Module {
	void onReset(const ResetEvent& e) override {
		_param_change_values.clear();
		rack::engine::Module::onReset(e);
	}

	inline bool paramChanged(std::size_t param_id) {
		float val = params[param_id].getValue();
		auto found = _param_change_values.find(param_id);
		if (found == _param_change_values.end() || found->second != val) {
			_param_change_values[param_id] = val;
			return true;
		}

		return false;
	}

private:
	std::map<const std::size_t, float> _param_change_values;
};

}  // namespace engine
}  // namespace slime
