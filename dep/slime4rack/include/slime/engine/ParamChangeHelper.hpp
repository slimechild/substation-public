/**********
 * Copyright (c) 2022, Coriander Violet Pines. All rights reserved.
 * This file is part of slime4rack and is governed by the attached license.
 *
 * Author: C. V. Pines
 * Status: deprecated
 */

#pragma once

#include <cstddef>
#include <map>

#include <engine/Module.hpp>

namespace slime {
namespace engine {

class ParamChangeHelper {
public:
	ParamChangeHelper(rack::engine::Module* module);

	void reset();
	bool changed(std::size_t id);
	float value(std::size_t id);

private:
	rack::engine::Module* _module;
	std::map<const std::size_t, float> _values;
};

}  // namespace engine
}  // namespace slime
