/**********
 * Copyright (c) 2022, Coriander Violet Pines. All rights reserved.
 * This file is part of slime4rack and is governed by the attached license.
 *
 * Author: C. V. Pines
 * Status: un-stable
 */

#pragma once

#include <slime/Invoker.hpp>

namespace slime {
namespace settings {

template <typename T>
class ValueChangeNotifier : public slime::Invoker<T> {
public:
	typedef T ValueType;

	void set(const T& value) {
		_value = value;
		slime::Invoker<T>::invoke(_value);
	}

	const T& get() const { return _value; }

	ValueChangeNotifier& operator=(T value) {
		set(value);
		return *this;
	}

	operator T() const { return get(); }

private:
	T _value;
};

#define JSON_GET_BOOL(obj, key)                       \
	do {                                              \
		json_t* __param = json_object_get(obj, #key); \
		if (__param) {                                \
			key = json_boolean_value(__param);        \
		}                                             \
	} while (0)

#define JSON_SET_BOOL(obj, key)                            \
	do {                                                   \
		json_object_set_new(obj, #key, json_boolean(key)); \
	} while (0)

#define JSON_GET_INT(obj, key)                        \
	do {                                              \
		json_t* __param = json_object_get(obj, #key); \
		if (__param) {                                \
			key = json_integer_value(__param);        \
		}                                             \
	} while (0)

#define JSON_SET_INT(obj, key)                             \
	do {                                                   \
		json_object_set_new(obj, #key, json_integer(key)); \
	} while (0)

}  // namespace settings
}  // namespace slime
