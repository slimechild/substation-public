/**********
 * Copyright (c) 2022, Coriander Violet Pines. All rights reserved.
 * This file is part of slime4rack and is governed by the attached license.
 *
 * Author: C. V. Pines
 * Status: stable
 */

#pragma once

#if __INTELLISENSE__

// Intellisense doesn't understand GCC attributes
#ifndef __attribute__
#define __attribute__(...)
#endif

#endif
