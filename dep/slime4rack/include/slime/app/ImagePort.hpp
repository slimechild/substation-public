/**********
 * Copyright (c) 2022, Coriander Violet Pines. All rights reserved.
 * This file is part of slime4rack and is governed by the attached license.
 *
 * Author: C. V. Pines
 * Status: stable
 * Compatibility: 2.0.0
 */

#pragma once

#include <vector>

#include <app/PortWidget.hpp>
#include <widget/FramebufferWidget.hpp>

#include "../widget/ImageWidget.hpp"

namespace slime {
namespace app {

// Implements an input/output port that displays a fixed image.
class ImagePort : public rack::app::PortWidget {
public:
	ImagePort();

	void setImage(const std::string& path, const rack::math::Vec& size);

private:
	rack::widget::FramebufferWidget* _fb = nullptr;
	widget::ImageWidget* _iw = nullptr;
};

}  // namespace app
}  // namespace slime
