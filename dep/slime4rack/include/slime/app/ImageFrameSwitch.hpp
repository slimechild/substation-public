/**********
 * Copyright (c) 2022, Coriander Violet Pines. All rights reserved.
 * This file is part of slime4rack and is governed by the attached license.
 *
 * Author: C. V. Pines
 * Status: stable
 * Compatibility: 2.0.0
 */

#pragma once

#include <cstddef>
#include <map>
#include <memory>
#include <utility>

#include <app/Switch.hpp>
#include <app/common.hpp>
#include <widget/FramebufferWidget.hpp>
#include <widget/Widget.hpp>

#include "../widget/ImageWidget.hpp"

namespace slime {
namespace app {

// Implements a switch that flips between a number of images.
class ImageFrameSwitch : public rack::app::Switch {
public:
	using FrameSet = std::vector<std::string>;

	ImageFrameSwitch();

	void setFrames(const FrameSet& frames, const rack::math::Vec& size);

	inline std::size_t getIndex() const { return _index; }

	void onChange(const rack::widget::Widget::ChangeEvent& e) override;

protected:
	std::size_t _index;
	rack::widget::FramebufferWidget* _fb = nullptr;
	widget::ImageWidget* _iw = nullptr;
	FrameSet _frames;
};

}  // namespace app
}  // namespace slime
