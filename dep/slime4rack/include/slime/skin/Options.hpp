/**********
 * Copyright (c) 2022, Coriander Violet Pines. All rights reserved.
 * This file is part of slime4rack and is governed by the attached license.
 *
 * Author: C. V. Pines
 * Status: stable
 * Compatibility: 1.1.0
 */

#pragma once

#include <string>

#include <slime/Assert.hpp>

#include <math.hpp>

namespace slime {
namespace skin {
namespace detail {

enum Orientation { HORIZONTAL = 1100, VERTICAL = 1101 };

enum Color {
	CLEAR = 2200,
	BLACK_PLASTIC = 2201,
	ORANGE = 2202,
	METAL = 2203,
	GREY = 2204,
	MINT = 2205,
	BLUE = 2206,
	GOLD = 2207,
	GREEN = 2208,
	RED = 2209,
	VIOLET = 2210
};

enum SwitchPosition { DOUBLE_THROW = 3300, TRIPLE_THROW = 3301 };

enum Size {
	SIZE_0_12_IN = 99012,
	SIZE_0_2_IN = 9902,
	SIZE_0_24_IN = 99024,
	SIZE_0_4_IN = 9904,
	SIZE_0_7_IN = 9907,
	SIZE_0_8_IN = 9908
};

std::string toString(const Orientation color);

std::string toString(const Color color);

std::string toString(const Size size);

rack::math::Vec toVec(const Size size);

}  // namespace detail
}  // namespace skin
}  // namespace slime
