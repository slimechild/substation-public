/**********
 * Copyright (c) 2022, Coriander Violet Pines. All rights reserved.
 * This file is part of slime4rack and is governed by the attached license.
 *
 * Author: C. V. Pines
 * Status: stable
 * Compatibility: 2.3.0
 */

#pragma once

#include <string>
#include <vector>

#include "../IntellisenseFix.hpp"

#include "../Common.hpp"
#include "../app/CableColorMixin.hpp"
#include "../app/ImageFrameSwitch.hpp"
#include "../app/ImageLight.hpp"
#include "../app/ImagePort.hpp"
#include "../app/SimpleImageKnob.hpp"
#include "Component.hpp"
#include "Options.hpp"

namespace slime {
namespace skin {
namespace ersatz {

// Knobs
struct Knob : component::KnobComponent<Knob, app::SimpleImageKnob> {
	enum class Size { SMALL = detail::SIZE_0_24_IN, MEDIUM = detail::SIZE_0_4_IN, BIG = detail::SIZE_0_8_IN };

	static Knob* makeComponent(const Size size) { return new Knob(size); }

private:
	Knob(const Size size);
};

// Switches
struct SlideSwitch : component::SwitchComponent<SlideSwitch, app::ImageFrameSwitch> {
	enum class Orientation { HORIZONTAL = detail::HORIZONTAL, VERTICAL = detail::VERTICAL };
	enum class Positions { DOUBLE_THROW = detail::DOUBLE_THROW, TRIPLE_THROW = detail::TRIPLE_THROW };

	static SlideSwitch* makeComponent(const Orientation orientation, const Positions positions) {
		return new SlideSwitch(orientation, positions);
	}

private:
	SlideSwitch(const Orientation orientation, const Positions positions);
};

// Ports
struct Port : component::PortComponent<Port, app::CableColorMixin<app::ImagePort>> {
	static Port* makeComponent() { return new Port(); }

	// Set the custom colors. Does not assume ownership.
	Port* setCustomColors(const std::shared_ptr<const std::vector<NVGcolor>>& colors) {
		app::CableColorMixin<app::ImagePort>::setCustomColors(colors);
		return static_cast<Port*>(this);
	}

private:
	Port();
};

// Test Build Warning
struct TestBuildWarning : component::TextLabel {
	void attach(rack::app::ModuleWidget* widget);

	static TestBuildWarning* makeComponent() { return new TestBuildWarning(); }

private:
	TestBuildWarning();
};

// Light
struct MonochromeLED : component::MonochromeLightComponent<MonochromeLED, app::ImageLight> {
	enum class Size { SMALL = detail::SIZE_0_12_IN, MEDIUM = detail::SIZE_0_2_IN };

	enum class Color { MINT = detail::GREY };

	static MonochromeLED* makeComponent(const Size size, const Color light_color) {
		return new MonochromeLED(size, light_color);
	}

private:
	MonochromeLED(const Size size, const Color light_color);
};

// Button
struct Button : component::SwitchComponent<Button, app::ImageFrameSwitch> {
	enum class Size { SMALL = detail::SIZE_0_2_IN, MEDIUM = detail::SIZE_0_4_IN };
	enum class OffColor { CLEAR = detail::CLEAR };
	enum class OnColor { GREY = detail::GREY };

	static Button* makeComponent(const Size size, const OffColor off_color, const OnColor on_color) {
		return new Button(size, off_color, on_color);
	}

private:
	Button(const Size size, const OffColor off_color, const OnColor on_color);
};

// Brand Logo
struct BrandLogo : component::SvgIcon {
	enum class Size { LOGO = detail::SIZE_0_2_IN, FULL = detail::SIZE_0_7_IN };

	void attach(rack::app::ModuleWidget* widget);

	static BrandLogo* makeComponent(const Size size) { return new BrandLogo(size); };

private:
	BrandLogo(const Size size);
	std::string getPath(const Size size);
};

}  // namespace ersatz
}  // namespace skin
}  // namespace slime
