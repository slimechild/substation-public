/**********
 * Copyright (c) 2022, Coriander Violet Pines. All rights reserved.
 * This file is part of slime4rack and is governed by the attached license.
 *
 * Author: C. V. Pines
 * Status: stable
 * Compatibility: 1.1.0
 */

#pragma once

#include <vector>

#include "../IntellisenseFix.hpp"

#include "../Common.hpp"
#include "../app/ImageFrameSwitch.hpp"
#include "../app/ImageLight.hpp"
#include "Component.hpp"
#include "Options.hpp"

#include <nanovg.h>

namespace slime {
namespace skin {
namespace substation {

// Light
struct MonochromeLED : component::MonochromeLightComponent<MonochromeLED, app::ImageLight> {
	enum class Size { SMALL = detail::SIZE_0_12_IN, MEDIUM = detail::SIZE_0_2_IN };

	enum class Color { MINT = detail::MINT };

	static MonochromeLED* makeComponent(const Size size, const Color light_color) {
		return new MonochromeLED(size, light_color);
	}

private:
	MonochromeLED(const Size size, const Color light_color);
};

// Button
struct Button : component::SwitchComponent<Button, app::ImageFrameSwitch> {
	enum class Size { SMALL = detail::SIZE_0_2_IN, MEDIUM = detail::SIZE_0_4_IN };
	enum class OffColor { GREY = detail::GREY };
	enum class OnColor { MINT = detail::MINT };

	static Button* makeComponent(const Size size, const OffColor off_color, const OnColor on_color) {
		return new Button(size, off_color, on_color);
	}

	void onChange(const rack::event::Change& e) override;

private:
	Button(const Size size, const OffColor off_color, const OnColor on_color);
};

// Brand Logo
struct BrandLogo : component::SvgIcon {
	enum class Size { LOGO = detail::SIZE_0_2_IN, FULL = detail::SIZE_0_7_IN };

	void attach(rack::app::ModuleWidget* widget);

	static BrandLogo* makeComponent(const Size size) { return new BrandLogo(size); };

private:
	BrandLogo(const Size size);
	std::string getPath(const Size size);
};

// Cable colors
extern const std::vector<NVGcolor> CABLE_COLORS;

}  // namespace substation
}  // namespace skin
}  // namespace slime
