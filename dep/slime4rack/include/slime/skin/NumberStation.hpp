/**********
 * Copyright (c) 2022, Coriander Violet Pines. All rights reserved.
 * This file is part of slime4rack and is governed by the attached license.
 *
 * Author: C. V. Pines
 * Status: stable
 * Compatibility: 2.0.0
 */

#pragma once

#include <string>
#include <vector>

#include "../IntellisenseFix.hpp"

#include "../app/CableColorMixin.hpp"
#include "../app/ImageFrameSwitch.hpp"
#include "../app/ImageLight.hpp"
#include "../app/ImagePort.hpp"
#include "../app/SimpleImageKnob.hpp"
#include "Component.hpp"
#include "Options.hpp"

namespace slime {
namespace skin {
namespace numberstation {

// Knobs
struct Knob : component::KnobComponent<Knob, app::SimpleImageKnob> {
	enum class Size { SMALL = detail::SIZE_0_24_IN, MEDIUM = detail::SIZE_0_4_IN, BIG = detail::SIZE_0_8_IN };

	static Knob* makeComponent(const Size size) { return new Knob(size); }

private:
	Knob(const Size size);
};

// Switches
struct SlideSwitch : component::SwitchComponent<SlideSwitch, app::ImageFrameSwitch> {
	enum class Orientation { HORIZONTAL = detail::HORIZONTAL, VERTICAL = detail::VERTICAL };
	enum class Positions { DOUBLE_THROW = detail::DOUBLE_THROW, TRIPLE_THROW = detail::TRIPLE_THROW };

	static SlideSwitch* makeComponent(const Orientation orientation, const Positions positions) {
		return new SlideSwitch(orientation, positions);
	}

private:
	SlideSwitch(const Orientation orientation, const Positions positions);
};

// Ports
struct Port : component::PortComponent<Port, app::CableColorMixin<app::ImagePort>> {
	static Port* makeComponent() { return new Port(); }

	// Set the custom colors. Does not assume ownership.
	Port* setCustomColors(const std::shared_ptr<const std::vector<NVGcolor>>& colors) {
		app::CableColorMixin<app::ImagePort>::setCustomColors(colors);
		return static_cast<Port*>(this);
	}

private:
	Port();
};

// Test Build Warning
struct TestBuildWarning : component::TextLabel {
	void attach(rack::app::ModuleWidget* widget);

	static TestBuildWarning* makeComponent() { return new TestBuildWarning(); }

private:
	TestBuildWarning();
};

}  // namespace numberstation
}  // namespace skin
}  // namespace slime
