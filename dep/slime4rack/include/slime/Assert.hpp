/**********
 * Copyright (c) 2022, Coriander Violet Pines. All rights reserved.
 * This file is part of slime4rack and is governed by the attached license.
 *
 * Author: C. V. Pines
 * Status: stable
 * Compatibility: 1.0.0
 */

#pragma once

#include <slime/IntellisenseFix.hpp>

#include <assert.h>
#include <initializer_list>

#include <logger.hpp>

#define assertEnumMsg(message, enum_t, value)                               \
	do {                                                                    \
		FATAL("Invalid value %i for enum \"" #enum_t "\" " message, value); \
		assert(false);                                                      \
	} while (0)

#define assertEnum(enum_t, value) assertEnumMsg("", enum_t, value)

#define assertInRangeMsg(message, x, ...)                                     \
	do {                                                                      \
		bool found = false;                                                   \
		for (const auto& v : __VA_ARGS__)                                     \
			if ((x) == (v)) {                                                 \
				found = true;                                                 \
				break;                                                        \
			}                                                                 \
		if (!found) {                                                         \
			FATAL("Value \"" #x "\" not in range " #__VA_ARGS__ " " message); \
			assert(false);                                                    \
		}                                                                     \
	} while (0)

#define assertInRange(x, ...) assertInRangeMsg("", x, __VA_ARGS__)

#define assertMsg(message, x)                           \
	do {                                                \
		if (!(x)) {                                     \
			FATAL("Assertion failed: " #x " " message); \
			assert(x);                                  \
		}                                               \
	} while (0)
