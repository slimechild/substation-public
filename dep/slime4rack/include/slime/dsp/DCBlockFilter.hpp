/**********
 * Copyright (c) 2022, Coriander Violet Pines. All rights reserved.
 * This file is part of slime4rack and is governed by the attached license.
 *
 * Author: C. V. Pines
 * Status: stable
 * Compatibility: 1.0.0
 */

#pragma once

#include <simd/functions.hpp>

namespace slime {
namespace dsp {

// Implements a DC blocking filter consisting of a differentiator followed by a leaky integrator.
template <typename T = float>
class DCBlockFilter {
public:
	T value;

	DCBlockFilter() { reset(); }

	void reset() {
		_xZ1 = static_cast<T>(0.0f);
		value = static_cast<T>(0.0f);
	}

	T process(T x) {
		T y = x - _xZ1 + static_cast<T>(0.999f) * value;
		_xZ1 = x;
		value = rack::simd::clamp(y, -10.0f, 10.0f);
		return value;
	}

private:
	T _xZ1;
};

}  // namespace dsp
}  // namespace slime
