/**********
 * Copyright (c) 2022, Coriander Violet Pines. All rights reserved.
 * This file is part of slime4rack and is governed by the attached license.
 *
 * Author: C. V. Pines
 * Status: stable
 * Compatibility: 1.0.0
 */

#pragma once

#include <simd/functions.hpp>

namespace slime {
namespace dsp {

// Implements the phase distortion synthesis model described in section 2 of
// "Discrete-Time Modelling of the Moog Sawtooth Oscillator Waveform"
// by Jussi Pekonen,  Victor Lazzarini, Joseph Timoney, Jari Kleimola, and Vesa Välimäki
template <typename T>
class PhaseDistortionSawOscillator {
public:
	T value = static_cast<T>(0.0f);

	PhaseDistortionSawOscillator() { reset(); }

	void reset() {
		value = static_cast<T>(0.0f);
		_phase = static_cast<T>(0.0f);
	}

	// Set the frequency in Hz
	void setFrequency(T freq) { _frequency = freq; }

	// Set the phase distortion amount, in [0.001, 1] where 0.5 is an undistorted sine wave
	void setDistortion(T d) { _distortion = rack::simd::clamp(d, 1.0e-3f, 1.0f); }

	// Automatically sets distortion in a way that approximates a Moog VCO
	void setFrequencyAndDisortion(T freq) {
		_frequency = freq;
		_distortion = static_cast<T>(0.9924f) - static_cast<T>(0.00002151f) * freq;
	}

	void sync() { _phase = static_cast<T>(0.0f); }

	// Advance oscillator by 1/Fs
	T process(float delta_time) {
		// Cap at 80% of the Nyquist frequency
		T delta_phase = rack::simd::fmin(_frequency * delta_time, 0.4f);

		_phase += delta_phase;
		_phase -= rack::simd::floor(_phase);

		T phi = M_PI * (static_cast<T>(1.0) - static_cast<T>(2.0) * _distortion) *
				rack::simd::ifelse(_phase < _distortion, _phase / _distortion,
								   (static_cast<T>(1.0) - _phase) / (static_cast<T>(1.0) - _distortion));

		value = -rack::simd::cos(static_cast<T>(2.0 * M_PI) * _phase + phi);
		return value;
	}

private:
	T _phase = 0.0f;
	T _frequency;
	T _distortion;
};

}  // namespace dsp
}  // namespace slime
