/**********
 * Copyright (c) 2022, Coriander Violet Pines. All rights reserved.
 * This file is part of slime4rack and is governed by the attached license.
 *
 * Author: C. V. Pines
 * Status: unstable
 */

#include <algorithm>
#include <array>
#include <cstddef>

#include <slime/Common.hpp>

namespace slime {
namespace dsp {

// A delay line with a compact memory footprint.
template <typename _Ty, std::size_t _Size>
class CompactDelayLine {
	static_assert((_Size & (_Size - 1)) == 0, SLIME_STRINGIFY(_Size) " must be a power of two!");

public:
	using value_type = _Ty;
	using size_type = std::size_t;
	using reference = _Ty&;
	using const_reference = const _Ty&;

	CompactDelayLine() { clear(); }

	inline static constexpr size_type length() { return _Size; }

	void clear() {
		_index = 0;
		_elems.fill(_Ty());
	}

	[[nodiscard]] reference operator[](const size_type delay) { return at(delay); }
	[[nodiscard]] const_reference operator[](const size_type delay) const { return at(delay); }

	void push(const_reference val) {
		_elems[_index] = val;
		_index = (_index + 1) & MASK;
	}

	reference at(const size_type delay) {
#if defined(SLIME_DEBUG)
		if (delay >= _Size) {
			FATAL("Invalid delay line index %d greater than length %d", (int)delay, (int)_Size);
			throw -1;
		}
#endif
		return _elems[(_index - delay - 1) & MASK];
	}

	const_reference at(const size_type delay) const {
#if defined(SLIME_DEBUG)
		if (delay >= _Size) {
			FATAL("Invalid delay line index %d greater than length %d", (int)delay, (int)_Size);
			throw -1;
		}
#endif
		return _elems[(_index - delay - 1) & MASK];
	}

private:
	static const size_type MASK = _Size - 1;

	size_type _index;
	std::array<_Ty, _Size> _elems;
};

// A delay line that uses double the storage but stores items in contiguous memory.
// For a size of 65536, a SlidingDelayLine is faster than a CompactDelayLine for 3+ reads per write.
template <typename _Ty, std::size_t _Size>
class SlidingDelayLine {
public:
	using value_type = _Ty;
	using size_type = std::size_t;
	using reference = _Ty&;
	using const_reference = const _Ty&;

	SlidingDelayLine() { clear(); }

	inline static size_type length() { return _Size; }

	void clear() { clear(value_type()); }

	void clear(value_type value) {
		_index = _Size;
		_buffer.fill(value);
	}

	[[nodiscard]] reference operator[](const size_type delay) { return at(delay); }
	[[nodiscard]] const_reference operator[](const size_type delay) const { return at(delay); }

	void push(const_reference val) {
		// Fill
		_buffer[_index] = val;

		// Copy and wrap
		if (__builtin_expect(_index < (BUFFER_SIZE - 1), 1))
			[[likely]] { _index++; }
		else {
			// optimizes to a single memcpy with -O3
			std::copy(_buffer.begin() + _Size, _buffer.end(), _buffer.begin());
			_index = _Size;
		}
	}

	reference at(const size_type delay) {
#if defined(SLIME_DEBUG)
		if (delay > _index) {
			FATAL("Invalid delay line index %d greater than length %d", (int)delay, (int)_Size);
			throw -1;
		}
#endif

		return _buffer[_index - delay];
	}

	const_reference at(const size_type delay) const {
#if defined(SLIME_DEBUG)
		if (delay > _index) {
			FATAL("Invalid delay line index %d greater than length %d", (int)delay, (int)_Size);
			throw -1;
		}
#endif

		return _buffer[_index - delay];
	}

	// Return a pointer to a contiguous block of memory containing the delay line's data.
	slime::span<_Ty, _Size> data() { return slime::span<_Ty, _Size>(_buffer.data() + _index - _Size); }
	slime::span<const _Ty, _Size> cdata() const {
		return slime::span<const _Ty, _Size>(_buffer.data() + _index - _Size);
	}

private:
	static const size_type BUFFER_SIZE = _Size << 1;

	size_type _index;
	std::array<_Ty, BUFFER_SIZE> _buffer;
};

// A variant of SlidingDelayLine that amortizes the copy step over time
// This version provides the best and most stable performance, at the expense of larger memory size
template <typename _Ty, std::size_t _Size>
class AmortizedDelayLine {
public:
	using value_type = _Ty;
	using size_type = std::size_t;
	using reference = _Ty&;
	using const_reference = const _Ty&;

	AmortizedDelayLine() { clear(); }

	inline static size_type length() { return _Size; }

	void clear() { clear(value_type()); }

	void clear(value_type value) {
		_index = _Size;
		_buffer.fill(value);
	}

	[[nodiscard]] reference operator[](const size_type delay) { return at(delay); }
	[[nodiscard]] const_reference operator[](const size_type delay) const { return at(delay); }

	void push(const_reference val) {
		// Fill
		_buffer[_index] = val;

		// Copy
		_buffer[_index - _Size] = _buffer[_index];

		// Wrap
		if (__builtin_expect(_index < (BUFFER_SIZE - 1), 1))
			[[likely]] { _index++; }
		else {
			_index = _Size;
		}
	}

	reference at(const size_type delay) {
#if defined(SLIME_DEBUG)
		if (delay > _index) {
			FATAL("Invalid delay line index %d greater than length %d", (int)delay, (int)_Size);
			throw -1;
		}
#endif

		return _buffer[_index - delay];
	}

	const_reference at(const size_type delay) const {
#if defined(SLIME_DEBUG)
		if (delay > _index) {
			FATAL("Invalid delay line index %d greater than length %d", (int)delay, (int)_Size);
			throw -1;
		}
#endif

		return _buffer[_index - delay];
	}

	// Return a pointer to a contiguous block of memory containing the delay line's data.
	slime::span<_Ty, _Size> data() { return slime::span<_Ty, _Size>(_buffer.data() + _index - _Size); }

private:
	static const size_type BUFFER_SIZE = _Size << 1;

	size_type _index;
	std::array<_Ty, BUFFER_SIZE> _buffer;
};

}  // namespace dsp
}  // namespace slime
