/**********
 * Copyright (c) 2022, Coriander Violet Pines. All rights reserved.
 * This file is part of slime4rack and is governed by the attached license.
 *
 * Author: C. V. Pines
 * Status: stable
 * Compatibility: 1.0.0
 */

#pragma once

#include <cmath>

#include <math.hpp>

namespace slime {
namespace dsp {

// Implements a basic clock generator. For clocking, not audio.
class ClockGenerator {
public:
	float value;

	ClockGenerator() { reset(); }

	void reset() {
		value = 0.0f;
		_phase = 0.0f;
	}

	// Set the frequency in Hz
	void setFrequency(float freq) { _frequency = freq; }

	// Reset phase to 0
	void sync() {
		_phase = 0.0f;
		value = 1.0f;
	}

	// Advance oscillator by 1/Fs
	float process(float delta_time) {
		// Cap at 80% of the Nyquist frequency, and prevent DC dividion-by-zero
		float delta_phase = rack::math::clamp(_frequency * delta_time, 1e-6f, 0.4f);

		// Increment phase
		_phase += delta_phase;
		_phase -= std::floor(_phase);

		// Calculate waveform
		value = (_phase < 0.5f) ? 1.0f : 0.0;
		return value;
	}

private:
	float _phase = 0.0f;
	float _frequency;
};

}  // namespace dsp
}  // namespace slime
