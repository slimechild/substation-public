/**********
 * Copyright (c) 2022, Coriander Violet Pines. All rights reserved.
 * This file is part of slime4rack and is governed by the attached license.
 *
 * Author: C. V. Pines
 * Status: stable
 * Compatibility: 1.0.0
 */

#pragma once

#include <dsp/filter.hpp>
#include <dsp/minblep.hpp>
#include <simd/functions.hpp>

namespace slime {
namespace dsp {

// Implements a bandlimited square wave using MinBLEP
template <typename T>
class MinBLEPSquareOscillator {
public:
	T value;
	int channels = 1;

	MinBLEPSquareOscillator() { reset(); }

	void reset() {
		value = static_cast<T>(0.0f);
		_phase = static_cast<T>(0.0f);
		_target = static_cast<T>(0.0f);
		_filter.reset();
	}

	// Set the frequency in Hz
	void setFrequency(T freq) { _frequency = freq; }

	// Set the pulse width, clamped to [10%, 90%]
	void setPulseWidth(T d) { _width = rack::simd::clamp(d, 0.1f, 0.9f); }

	void sync() { _phase = static_cast<T>(0.0f); }

	// Advance oscillator by 1/Fs
	T process(float delta_time) {
		// Cap at 80% of the Nyquist frequency, and prevent DC dividion-by-zero
		T delta_phase = rack::simd::clamp(_frequency * delta_time, 1e-6f, 0.4f);

		_phase += delta_phase;
		_phase -= rack::simd::floor(_phase);

		// Calculate waveform
		value = rack::simd::ifelse(_phase < _width, 1.0f, -1.0f);
		insertDiscontinuities(value, delta_phase);
		value += _minblep_generator.process();

		// Filter highpass
		_filter.setCutoffFreq(15.0f * delta_time);  // 15 Hz approx. matches Behringer Neutron
		_filter.process(value);
		value = _filter.highpass();

		return value;
	}

private:
	T _target = 0.0f;
	T _phase = 0.0f;
	T _frequency;
	T _width = 0.5f;

	rack::dsp::TRCFilter<T> _filter;
	rack::dsp::MinBlepGenerator<16, 16, T> _minblep_generator;

	inline void insertDiscontinuities(T target, T delta_phase);
};

template <>
inline void MinBLEPSquareOscillator<float>::insertDiscontinuities(float target, float delta_phase) {
	// Only calculate if a discontinuity has actually occurred.
	if (target == _target) {
		_target = target;
		return;
	}
	_target = target;

	// Insert discontinuity where phase crosses 0
	float zero_cross = (delta_phase - _phase) / delta_phase;
	if ((0.0f < zero_cross) && (zero_cross <= 1.0f)) {
		_minblep_generator.insertDiscontinuity(zero_cross - 1.0f, 2.0f);
	}

	// Insert discontinuity where phase crosses pulse width
	float pulse_cross = (_width - (_phase - delta_phase)) / delta_phase;
	if ((0.0f < pulse_cross) && (pulse_cross <= 1.0f)) {
		_minblep_generator.insertDiscontinuity(pulse_cross - 1.0f, -2.0f);
	}
}

template <typename T>
inline void MinBLEPSquareOscillator<T>::insertDiscontinuities(T target, T delta_phase) {
	// Only calculate if a discontinuity has actually occurred
	int change_mask = rack::simd::movemask(target != _target);
	_target = target;
	if (change_mask == 0) {
		return;
	}

	// SIMD code adapted from VCV's Fundamental VCO, with some extra work to prevent duplicates

	// Insert discontinuity where phase crosses 0
	T zero_cross = (delta_phase - _phase) / delta_phase;
	int zero_mask = change_mask & rack::simd::movemask((0.0f < zero_cross) & (zero_cross <= 1.0f));
	if (zero_mask) {
		for (int i = 0; i < channels; i++) {
			if (zero_mask & (1 << i)) {
				T mask = rack::simd::movemaskInverse<T>(1 << i);
				float p = zero_cross[i] - 1.0f;
				T x = mask & static_cast<T>(2.0f);
				_minblep_generator.insertDiscontinuity(p, x);
			}
		}
	}

	// Insert discontinuity where phase crosses pulse width
	T pulse_cross = (_width - (_phase - delta_phase)) / delta_phase;
	int pulse_mask = change_mask & rack::simd::movemask((0.0f < pulse_cross) & (pulse_cross <= 1.0f));
	pulse_mask = pulse_mask & ~zero_mask;  // Don't double-insert! This is probably overkill but it's cheap
	if (pulse_mask) {
		for (int i = 0; i < channels; i++) {
			if (pulse_mask & (1 << i)) {
				T mask = rack::simd::movemaskInverse<T>(1 << i);
				float p = pulse_cross[i] - 1.0f;
				T x = mask & static_cast<T>(-2.0f);
				_minblep_generator.insertDiscontinuity(p, x);
			}
		}
	}
}

}  // namespace dsp
}  // namespace slime
