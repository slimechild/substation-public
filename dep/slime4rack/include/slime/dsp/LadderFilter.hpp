/**********
 * Copyright (c) 2022, Coriander Violet Pines. All rights reserved.
 * This file is part of slime4rack and is governed by the attached license.
 *
 * Author: C. V. Pines
 * Status: stable
 * Compatibility: 1.1.0
 */

#pragma once

#include <array>
#include <cmath>
#include <cstddef>

#include <slime/Common.hpp>
#include <slime/Math.hpp>

#include <dsp/approx.hpp>
#include <dsp/ode.hpp>
#include <logger.hpp>
#include <math.hpp>
#include <simd/functions.hpp>

namespace slime {
namespace dsp {

// Moog-like 4 pole ladder lowpass with non-linear behaviour.
template <typename T = float>
class FourPoleLadderLowpass {
public:
	FourPoleLadderLowpass() { reset(); }

	void reset() {
		_xZ1 = static_cast<T>(0.0f);
		_state.fill(static_cast<T>(0.0f));
		_dS.fill(static_cast<T>(0.0f));
	}

	void process(float delta_time, T in) {
		using namespace slime::math;

		float inv_oversample = 1.0f / static_cast<float>(_oversample);
		float dt = delta_time * inv_oversample;

		T g = M_PI * _frequency * dt;
		T r = _resonance * (1.0f - 0.666f * _frequency / 20000.0f);  // 1st order empirical fit

		T dS, S;
		for (std::size_t i = 0; i < _oversample; i++) {
			T x = rack::simd::crossfade(_xZ1, in, static_cast<float>(i + 1) * inv_oversample);

			// Stage 1
			dS = g * (-_clip * tanh_rational5(_inv_clip * (x + r * _state[3])) - _state[0]);
			S = _state[0] + dS + _dS[0];  // 0.5 coeff included in g
			_dS[0] = dS;

			// Stage 2
			dS = g * (S - _state[1]);
			_state[0] = S;
			S = _state[1] + dS + _dS[1];
			_dS[1] = dS;

			// Stage 3
			dS = g * (S - _state[2]);
			_state[1] = S;
			S = _state[2] + dS + _dS[2];
			_dS[2] = dS;

			// Stage 4
			dS = g * (S - _clip * tanh_rational5(_inv_clip * _state[3]));
			_state[2] = S;
			_state[3] = _state[3] + dS + _dS[3];
			_dS[3] = dS;
		}

		for (std::size_t i = 0; i < _state.size(); i++) {
			_state[i] = rack::simd::clamp(_state[i], -10.0f, 10.0f);
			_dS[i] = rack::simd::clamp(_dS[i], -5.0f, 5.0f);  // dS carries a 0.5 multiplier
		}

		_xZ1 = in;
	}

	T lowpass1() const { return -_state[0] * _res_comp; }

	T lowpass2() const { return -_state[1] * _res_comp; }

	T lowpass3() const { return -_state[2] * _res_comp; }

	T lowpass4() const { return -_state[3] * _res_comp; }

	T bandpass2() const { return _state[0] - _state[1]; }

	T bandpass4() const { return 2.0f * (_state[1] - 2.0f * _state[2] + _state[3]); }

	void setCutoffFrequency(T freq) { _frequency = freq; }

	void setResonance(T res) {
		_resonance = 4.0f * res;
		_res_comp = (1.0f + 0.25f * _resonance);  // 1st order empirical fit
	}

	void setOversample(std::size_t ovr) { _oversample = std::min(ovr, static_cast<std::size_t>(32u)); }
	std::size_t getOversample() const { return _oversample; }

private:
	static constexpr const float _clip = 8.0f;
	static constexpr const float _inv_clip = 1.0f / _clip;

	std::size_t _oversample = 6u;

	T _resonance = 0.0f;
	T _frequency = 1000.0f;
	T _res_comp = 0.0f;

	T _xZ1;
	std::array<T, 4> _state;
	std::array<T, 4> _dS;
};

}  // namespace dsp
}  // namespace slime
