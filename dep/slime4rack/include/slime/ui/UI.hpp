/**********
 * Copyright (c) 2022, Coriander Violet Pines. All rights reserved.
 * This file is part of slime4rack and is governed by the attached license.
 *
 * Author: C. V. Pines
 * Status: stable
 * Compatibility: v2.0.0
 */

#include <memory>
#include <string>
#include <utility>
#include <vector>

#include <slime/IntellisenseFix.hpp>
#include <slime/Invoker.hpp>

#include <app/Scene.hpp>
#include <context.hpp>
#include <engine/Engine.hpp>
#include <history.hpp>
#include <math.hpp>
#include <settings.hpp>
#include <system.hpp>
#include <ui/MenuItem.hpp>
#include <ui/MenuSeparator.hpp>
#include <ui/common.hpp>

namespace slime {
namespace ui {

class BangMenuItem : public rack::ui::MenuItem {
public:
	BangMenuItem() { rightText = "!!"; }

	slime::Invoker<>& getInvoker() { return _invoker; }

	void onAction(const rack::event::Action& e) override { _invoker.invoke(); }

private:
	slime::Invoker<> _invoker;
};

template <typename T = bool>
class BoolSettingMenuItem : public rack::ui::MenuItem {
public:
	BoolSettingMenuItem(const std::shared_ptr<T>& setting) { _setting = setting; }

	slime::Invoker<T>& getInvoker() { return _invoker; }

	void onAction(const rack::event::Action& e) override {
		if (std::shared_ptr<T> setting_locked = _setting.lock()) {
			*setting_locked = !static_cast<bool>(*setting_locked);
			_invoker.invoke(*setting_locked);
		}
	}

	void step() override {
		if (std::shared_ptr<T> setting_locked = _setting.lock()) {
			rightText = CHECKMARK(static_cast<bool>(*setting_locked));
		}

		rack::ui::MenuItem::step();
	}

private:
	slime::Invoker<T> _invoker;
	std::weak_ptr<T> _setting;
};

template <typename T>
class MapChoiceMenuItem : public rack::ui::MenuItem {
public:
	MapChoiceMenuItem(const std::shared_ptr<T>& setting, const std::vector<std::pair<T, std::string>>& values) {
		_setting = setting;
		_values = values;
		this->rightText = RIGHT_ARROW;
	}

	slime::Invoker<T>& getInvoker() { return _invoker; }

	rack::ui::Menu* createChildMenu() override {
		rack::ui::Menu* menu = new rack::ui::Menu;

		for (const auto& p : _values) {
			SetValueMenuItem* item = new SetValueMenuItem(this, p.first, p.second);
			menu->addChild(item);
		}

		return menu;
	}

private:
	slime::Invoker<T> _invoker;
	std::vector<std::pair<T, std::string>> _values;
	std::weak_ptr<T> _setting;

	struct SetValueMenuItem : rack::ui::MenuItem {
		MapChoiceMenuItem* owner;
		const T& value;

		SetValueMenuItem(MapChoiceMenuItem* _owner, const T& _value, const std::string& _text)
			: owner(_owner), value(_value) {
			this->text = _text;
		}

		void onAction(const rack::event::Action& e) override {
			if (std::shared_ptr<T> setting_locked = owner->_setting.lock()) {
				*setting_locked = value;
				owner->_invoker.invoke(*setting_locked);
			}
		}

		void step() override {
			if (std::shared_ptr<T> setting_locked = owner->_setting.lock()) {
				rightText = CHECKMARK(value == *setting_locked);
			} else {
				rightText = "";
			}
			rack::ui::MenuItem::step();
		}
	};
};

class ModuleInstantionMenuItem : public rack::ui::MenuItem {
public:
	rack::app::ModuleWidget* module_widget;
	rack::plugin::Model* model;

	ModuleInstantionMenuItem() { rightText = "!!"; }

	void onAction(const rack::event::Action& e) override {
		rack::math::Rect box = module_widget->box;
		rack::math::Vec pos = box.pos.plus(rack::math::Vec(box.size.x, 0));

		// Update ModuleInfo if possible
		rack::settings::ModuleInfo* mi = rack::settings::getModuleInfo(model->plugin->slug, model->slug);
		if (mi) {
			mi->added++;
			mi->lastAdded = rack::system::getUnixTime();
		}

		// The below flow comes from rack::app::browser::chooseModel

		// Create module
		rack::engine::Module* new_module = model->createModule();
		if (!new_module) {
			WARN("Could not instantiate new module via menu item");
			return;
		}
		APP->engine->addModule(new_module);

		// Create module widget
		rack::app::ModuleWidget* new_widget = model->createModuleWidget(new_module);
		if (!new_widget) {
			WARN("Could not instantiate new module widget via menu item");
			delete new_module;
			return;
		}
		APP->scene->rack->setModulePosNearest(new_widget, pos);
		APP->scene->rack->addModule(new_widget);

		// Load template preset
		new_widget->loadTemplate();

		// Record history manually; can't use a guard here!
		rack::history::ModuleAdd* h = new rack::history::ModuleAdd;
		h->name = "create expander module";
		h->setModule(new_widget);
		APP->history->push(h);
	}
};

}  // namespace ui
}  // namespace slime
