/**********
 * Copyright (c) 2022, Coriander Violet Pines. All rights reserved.
 * This file is part of slime4rack and is governed by the attached license.
 *
 * Author: C. V. Pines
 * Status: stable
 * Compatibility: 1.0.0
 */

#pragma once

namespace slime {
namespace cv {

// Detects rising and falling edges.
template <typename T = float>
struct TSchmittTrigger {
	T state;
	T rising;
	T falling;
	T high = 2.0f;
	T low = 0.1f;

	TSchmittTrigger() { reset(); }

	void reset() { state = T::mask(); }

	T process(T in) {
		T on = (in >= high);
		T off = (in <= low);
		rising = ~state & on;
		falling = state & off;
		state = on | (state & ~off);
		return rising | falling;
	}
};

template <>
struct TSchmittTrigger<float> {
	bool state = true;
	bool rising = false;
	bool falling = false;
	float high = 2.0f;
	float low = 0.1f;

	void reset() { state = true; }

	bool process(float in) {
		bool on = (in >= high);
		bool off = (in <= low);
		rising = on && !state;
		falling = off && state;
		state = on || (state && !off);
		return rising || falling;
	}

	bool isHigh() { return state; }

	bool isRising() { return rising; }

	bool isFalling() { return falling; }

	bool changed() { return rising || falling; }
};

typedef TSchmittTrigger<> SchmittTrigger;

}  // namespace cv
}  // namespace slime