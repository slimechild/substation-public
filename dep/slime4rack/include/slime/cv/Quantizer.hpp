/**********
 * Copyright (c) 2022, Coriander Violet Pines. All rights reserved.
 * This file is part of slime4rack and is governed by the attached license.
 *
 * Author: C. V. Pines
 * Status: stable
 * Compatibility: 1.0.0
 */

#pragma once

#include <array>
#include <cmath>
#include <cstddef>

#include <slime/Common.hpp>

#include <logger.hpp>

namespace slime {
namespace cv {

// Quantizes notes to a scale.
class Quantizer {
public:
	// Equal-temperament chromatic scale
	static const std::array<float, 12> CHROMATIC_SCALE_ET;

	// Equal-tempermant diatonic scale
	static const std::array<float, 8> DIATONIC_SCALE_ET;

	// 12-tone justly-intonated chromatic scale using asymmetic 5-limit method
	static const std::array<float, 12> CHROMATIC_SCALE_JI;

	// 7-tone justly-intonated diatonic scale using 5-limit method; "Ptolemy's intense diatonic scale"
	static const std::array<float, 8> DIATONIC_SCALE_JI;

	Quantizer() { setScale(Quantizer::CHROMATIC_SCALE_ET); }

	// Set the scale. Does not assume ownership.
	void setScale(const slime::span<const float>& scale);

	// Quantize a note to the scale, with an optional root note.
	float process(float input, float root = 0.0f) const;

private:
	slime::span<const float> _scale;
};

}  // namespace cv
}  // namespace slime
