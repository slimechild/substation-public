/**********
 * Copyright (c) 2022, Coriander Violet Pines. All rights reserved.
 * This file is part of slime4rack and is governed by the attached license.
 *
 * Author: C. V. Pines
 * Status: stable
 * Compatibility: 1.0.0
 */

#pragma once

#include <array>
#include <cstddef>

namespace slime {
namespace cv {

// Quickly calculates up to N overtones (harmonics) of a pitch.
template <std::size_t N>
class Overtone {
public:
	Overtone() {
		for (std::size_t i = 0; i < _partials.size(); i++) {
			_partials[i] = std::log2f(static_cast<float>(i + 1));
		}
	}

	template <typename T>
	T getPartial(T in, std::size_t partial) const {
		return in + _partials[partial];
	}

private:
	std::array<float, N> _partials;
};

// Quickly calculates up to N undertones (subharmonics) of a pitch.
template <std::size_t N>
class Undertone {
public:
	Undertone() : _partials() {
		for (std::size_t i = 0; i < _partials.size(); i++) {
			_partials[i] = std::log2f(static_cast<float>(i + 1));
		}
	}

	template <typename T>
	T getPartial(T in, std::size_t partial) const {
		return in - _partials[partial];
	}

private:
	std::array<float, N> _partials;
};

}  // namespace cv
}  // namespace slime