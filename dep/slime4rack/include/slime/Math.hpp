/**********
 * Copyright (c) 2022, Coriander Violet Pines. All rights reserved.
 * This file is part of slime4rack and is governed by the attached license.
 *
 * Author: C. V. Pines
 * Status: stable
 * Compatibility: 2.0.0
 *
 * FUTURE: stuff from https://github.com/Rcomian/rcm-modules/blob/v1/src/ladspa-util.h
 */

#pragma once
#include <engine/Port.hpp>
#include <simd/functions.hpp>

namespace slime {
namespace math {

static const double LOG_2_10 = 3.3219280948873623478703194294894;

// Hyperbolic tangent function
template <typename T = float>
T tanh(T x) {
	T ex = rack::simd::exp(static_cast<T>(2.0) * x);
	return (ex - static_cast<T>(1.0)) / (ex + static_cast<T>(1.0));
}

// Fifth-order rational approximation to tanh
inline float tanh_rational5(float x) {
	if (x <= -3.0f) {
		return -1.0f;
	}

	if (x >= 3.0f) {
		return 1.0f;
	}

	float x2 = x * x;
	return x * (27.0f + x2) / (27.0f + 9.0f * x2);
}

// Fifth-order rational approximation to tanh (simd)
template <typename T>
T tanh_rational5(T x) {
	T overmask = (x >= static_cast<T>(3.0));
	T undermask = (x <= static_cast<T>(-3.0));
	T x2 = x * x;
	x = x * (static_cast<T>(27.0) + x2) / (static_cast<T>(27.0) + static_cast<T>(9.0) * x2);
	return rack::simd::ifelse(overmask, static_cast<T>(1.0), rack::simd::ifelse(undermask, static_cast<T>(-1.0), x));
}

// Third-order polynomial approximation to sinc(x - 1), on [0, 1]
template <typename T = float>
T shifted_sinc_poly3(T x) {
	return x * (static_cast<T>(-1.0) + x * (static_cast<T>(1.0) - x));
}

// SIMD
typedef rack::simd::float_4 float_simd;
const size_t SIMD_PAR = rack::engine::PORT_MAX_CHANNELS / float_simd::size;

}  // namespace math
}  // namespace slime
