/**********
 * Copyright (c) 2022, Coriander Violet Pines. All rights reserved.
 * This file is part of slime4rack and is governed by the attached license.
 *
 * Author: C. V. Pines
 * Status: stable
 * Compatibility: 1.0.0
 */

#pragma once

#include <nanovg.h>
#include <math.hpp>
#include <widget/Widget.hpp>

namespace slime {
namespace widget {

class SolidRectangleWidget : public rack::widget::Widget {
public:
	void draw(const DrawArgs& args) override;
	inline void setColor(const NVGcolor& color) { _color = color; };
	inline void setColor(float r, float g, float b, float a = 1.0f) { _color = nvgRGBAf(r, g, b, a); };

private:
	NVGcolor _color = nvgRGBf(1.0f, 1.0f, 1.0f);
};

}  // namespace widget
}  // namespace slime
