/**********
 * Copyright (c) 2022, Coriander Violet Pines. All rights reserved.
 * This file is part of slime4rack and is governed by the attached license.
 *
 * Author: C. V. Pines
 * Status: stable
 * Compatibility: 2.0.0
 */

#pragma once

#include <memory>
#include <utility>

#include <slime/IntellisenseFix.hpp>

#include <common.hpp>
#include <widget/Widget.hpp>
#include <window/Window.hpp>

namespace slime {
namespace widget {

using OversampledImage = std::pair<std::string, float>;

// Widget that draws an image.
class ImageWidget : public rack::widget::Widget {
public:
	enum class BlendMode {
		NORMAL_PREMUL,  // s=ONE, d=ONE_MINUS_SRC_ALPHA, sa=ONE, da=ONE_MINUS_SRC_ALPHA
		NORMAL,         // s=SRC_ALPHA, d=ONE_MINUS_SRC_ALPHA, sa=ONE, da=ONE_MINUS_SRC_ALPHA
		ADD,            // s=SRC_ALPHA, d=ONE, sa=ONE, da=ONE
		MULTIPLY,       // s=DST_COLOR, d=ZERO, sa=ONE, da=ZERO
		SCREEN,         // s=ONE_MINUS_DST, d=ONE, sa=ONE, da=ONE
	};

	void setImage(const std::string& path);
	inline void setBlendMode(BlendMode mode) { _blend = mode; }
	inline void setAlpha(float alpha) { _alpha = rack::math::clamp(alpha, 0.0f, 1.0f); }
	inline bool hasImage() const { return !_imagePath.empty(); };
	inline void useDrawLayerMethod(int layer) { _layer = layer; }
	inline void useDrawMethod() { _layer = _USE_DRAW_METHOD_LAYER; }

	void resize(float oversample = 1.0f);

	void draw(const DrawArgs& args) override;
	void drawLayer(const DrawArgs& args, int layer) override;

	void onContextDestroy(const ContextDestroyEvent& e) override;
	// void onContextCreate(const ContextCreateEvent& e) override;  // This doesn't seem to work

protected:
	static const int _USE_DRAW_METHOD_LAYER = -999999;

	std::string _imagePath;
	std::shared_ptr<rack::window::Image> _imageData = nullptr;

	float _alpha = 1.0f;
	BlendMode _blend = BlendMode::NORMAL_PREMUL;
	int _layer = _USE_DRAW_METHOD_LAYER;

private:
	inline void loadImageData();
	inline void drawInternal(const DrawArgs& args);
};

}  // namespace widget
}  // namespace slime
