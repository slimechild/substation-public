/**********
 * Copyright (c) 2022, Coriander Violet Pines. All rights reserved.
 * This file is part of slime4rack and is governed by the attached license.
 *
 * Author: C. V. Pines
 */

#include <slime/Color.hpp>

namespace slime {

NVGcolor hexToColor(const std::string& color) {
	float r = std::stoi(color.substr(0, 2), 0, 16) * (1.0f / 255.0f);
	float g = std::stoi(color.substr(2, 2), 0, 16) * (1.0f / 255.0f);
	float b = std::stoi(color.substr(4, 2), 0, 16) * (1.0f / 255.0f);
	float a = 1.0f;
	if (color.size() > 6) {
		a = std::stoi(color.substr(6, 2), 0, 16) * (1.0f / 255.0f);
	}

	return nvgRGBAf(r, g, b, a);
}

}  // namespace slime
