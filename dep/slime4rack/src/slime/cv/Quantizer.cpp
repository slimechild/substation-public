/**********
 * Copyright (c) 2022, Coriander Violet Pines. All rights reserved.
 * This file is part of slime4rack and is governed by the attached license.
 *
 * Author: C. V. Pines
 */

#include <slime/cv/Quantizer.hpp>

namespace slime {
namespace cv {

const std::array<float, 12> Quantizer::CHROMATIC_SCALE_ET = {
	0.0f,         1.0f / 12.0f, 2.0f / 12.0f, 3.0f / 12.0f, 4.0f / 12.0f,  5.0f / 12.0f,
	6.0f / 12.0f, 7.0f / 12.0f, 8.0f / 12.0f, 9.0f / 12.0f, 10.0f / 12.0f, 11.0f / 12.0f,
};

const std::array<float, 8> Quantizer::DIATONIC_SCALE_ET = {0.0f,         2.0f / 12.0f, 4.0f / 12.0f, 5.0f / 12.0f,
														   7.0f / 12.0f, 9.0f / 12.0f, 11.0f / 12.0f};

const std::array<float, 12> Quantizer::CHROMATIC_SCALE_JI = {
	0.0f,          1.12f / 12.0f, 2.04f / 12.0f, 3.16f / 12.0f, 3.86f / 12.0f,  4.98f / 12.0f,
	5.90f / 12.0f, 7.02f / 12.0f, 8.14f / 12.0f, 8.84f / 12.0f, 10.18f / 12.0f, 10.88f / 12.0f};

const std::array<float, 8> Quantizer::DIATONIC_SCALE_JI = {0.0f,          2.04f / 12.0f, 3.86f / 12.0f, 4.98f / 12.0f,
														   7.02f / 12.0f, 8.84f / 12.0f, 10.88f / 12.0f};

void Quantizer::setScale(const slime::span<const float>& scale) {
	_scale = scale;
}

float Quantizer::process(float input, float root) const {
	float octave = 0.0f;
	float in_note = std::modf(input - root, &octave);

	if (in_note < 0.0f) {
		in_note += 1.0f;
		octave -= 1.0f;
	}

	float dist;
	float best_distance = 1e3f;
	float out_note = in_note;

	// Lowest note, octave up
	dist = std::abs(_scale[0] + 1.0f - in_note);
	if (dist < best_distance) {
		best_distance = dist;
		out_note = _scale[0] + 1.0f;
	}

	// Highest note, octave down
	dist = std::abs(_scale[_scale.size() - 1] - 1.0f - in_note);
	if (dist < best_distance) {
		best_distance = dist;
		out_note = _scale[_scale.size() - 1] - 1.0f;
	}

	// Closest in octave
	for (const float& note : _scale) {
		dist = std::abs(note - in_note);
		if (dist < best_distance) {
			best_distance = dist;
			out_note = note;
		}
	}

	return out_note + octave + root;
}

}  // namespace cv
}  // namespace slime
