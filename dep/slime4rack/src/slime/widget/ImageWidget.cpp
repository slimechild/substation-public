/**********
 * Copyright (c) 2022, Coriander Violet Pines. All rights reserved.
 * This file is part of slime4rack and is governed by the attached license.
 *
 * Author: C. V. Pines
 */

#include <slime/widget/ImageWidget.hpp>

#include <nanovg.h>
#include <app/common.hpp>
#include <context.hpp>
#include <math.hpp>

namespace slime {
namespace widget {

void ImageWidget::setImage(const std::string& path) {
	if (path == _imagePath)
		return;

	_imagePath = path;
	_imageData.reset();
}

void ImageWidget::resize(float oversample) {
	// Only works after we've drawn at least one frame! Will do nothing otherwise.
	if (hasImage() && _imageData) {
		int imgWidth, imgHeight;
		nvgImageSize(_imageData->vg, _imageData->handle, &imgWidth, &imgHeight);
		box.size =
			rack::math::Vec(static_cast<float>(imgWidth) / oversample, static_cast<float>(imgHeight) / oversample);
	}
}

void ImageWidget::draw(const DrawArgs& args) {
	if (_layer != ImageWidget::_USE_DRAW_METHOD_LAYER)
		return;

	ImageWidget::drawInternal(args);
}

void ImageWidget::drawLayer(const DrawArgs& args, int layer) {
	if (_layer == layer) {
		ImageWidget::drawInternal(args);
	}

	rack::widget::Widget::drawLayer(args, layer);
}

void ImageWidget::onContextDestroy(const ContextDestroyEvent& e) {
	// Cannot have any references across context re-creation, so release here
	_imageData.reset();
	Widget::onContextDestroy(e);
};

void ImageWidget::loadImageData() {
	// FUTURE: It would be great to keep the source file loaded in memory and just recreate the NVG context
	_imageData = rack::contextGet()->window->loadImage(_imagePath);
	if (!_imageData) {
		_imageData.reset();
		_imagePath = "";
	}
}

void ImageWidget::drawInternal(const DrawArgs& args) {
	if ((_alpha <= 0.0f) || !hasImage()) {
		return;
	}

	if (!_imageData) {
		// This will reload the image if the context was destroyed
		loadImageData();

		// Failed to load
		if (!_imageData) {
			return;
		}
	}

	nvgBeginPath(args.vg);
	NVGpaint imgPaint = nvgImagePattern(args.vg, 0.0f, 0.0f, box.size.x, box.size.y, 0.0f, _imageData->handle, 1.0f);
	nvgRect(args.vg, 0.0f, 0.0f, box.size.x, box.size.y);
	nvgFillPaint(args.vg, imgPaint);

	if (_blend != BlendMode::NORMAL_PREMUL) {
		// s=ONE, d=ONE_MINUS_SRC_ALPHA, sa=ONE, da=ONE_MINUS_SRC_ALPHA
		int s = NVGblendFactor::NVG_ONE;
		int d = NVGblendFactor::NVG_ONE_MINUS_SRC_ALPHA;
		int sa = NVGblendFactor::NVG_ONE;
		int da = NVGblendFactor::NVG_ONE_MINUS_SRC_ALPHA;

		switch (_blend) {
			case BlendMode::NORMAL:
				s = NVGblendFactor::NVG_SRC_ALPHA;
				d = NVGblendFactor::NVG_ONE_MINUS_SRC_ALPHA;
				sa = NVGblendFactor::NVG_ONE;
				da = NVGblendFactor::NVG_ONE_MINUS_SRC_ALPHA;
				break;

			case BlendMode::ADD:
				s = NVGblendFactor::NVG_SRC_ALPHA;
				d = NVGblendFactor::NVG_ONE;
				sa = NVGblendFactor::NVG_ONE;
				da = NVGblendFactor::NVG_ONE;
				break;

			case BlendMode::MULTIPLY:
				s = NVGblendFactor::NVG_DST_COLOR;
				d = NVGblendFactor::NVG_ZERO;
				sa = NVGblendFactor::NVG_ONE;
				da = NVGblendFactor::NVG_ONE;
				break;

			case BlendMode::SCREEN:
				s = NVGblendFactor::NVG_ONE_MINUS_DST_COLOR;
				d = NVGblendFactor::NVG_ONE;
				sa = NVGblendFactor::NVG_ONE;
				da = NVGblendFactor::NVG_ONE;

			case BlendMode::NORMAL_PREMUL:
			default:
				break;
		}

		nvgGlobalCompositeBlendFuncSeparate(args.vg, s, d, sa, da);
	}

	if (_alpha < 1.0f) {
		nvgGlobalAlpha(args.vg, _alpha);
	}

	nvgFill(args.vg);
	nvgClosePath(args.vg);
}

}  // namespace widget
}  // namespace slime
