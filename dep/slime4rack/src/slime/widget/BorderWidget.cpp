/**********
 * Copyright (c) 2022, Coriander Violet Pines. All rights reserved.
 * This file is part of slime4rack and is governed by the attached license.
 *
 * Author: C. V. Pines
 */

#include <slime/widget/BorderWidget.hpp>

namespace slime {
namespace widget {

void BorderWidget::draw(const DrawArgs& args) {
	nvgBeginPath(args.vg);
	nvgRect(args.vg, 0.5, 0.5, box.size.x - 1.0, box.size.y - 1.0);
	nvgStrokeColor(args.vg, _color);
	nvgStrokeWidth(args.vg, 1.0);
	nvgStroke(args.vg);
	nvgClosePath(args.vg);
}

}  // namespace widget
}  // namespace slime
