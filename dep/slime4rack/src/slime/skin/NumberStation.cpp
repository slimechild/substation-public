/**********
 * Copyright (c) 2022, Coriander Violet Pines. All rights reserved.
 * This file is part of slime4rack and is governed by the attached license.
 *
 * Author: C. V. Pines
 */

#include <slime/skin/NumberStation.hpp>

#include <slime/Assert.hpp>

#include <memory>
#include <string>
#include <utility>

#include <nanovg.h>
#include <app/common.hpp>
#include <asset.hpp>
#include <helpers.hpp>
#include <plugin/Plugin.hpp>
#include <window/Svg.hpp>

extern rack::plugin::Plugin* pluginInstance;

namespace slime {
namespace skin {
namespace numberstation {

/* Skin constants */
const std::string RESOURCE_SUFFIX = "4x.png";
const std::string COMPONENT_RESOURCE_DIR = "res/slime4rack/skin/numberstation/";
static const auto CABLE_COLORS = std::make_shared<const std::vector<NVGcolor>>(
	std::vector<NVGcolor>({nvgRGBAf(0.57f, 0.49f, 0.40f, 1.00f), nvgRGBAf(0.82f, 0.59f, 0.40f, 1.00f),
						   nvgRGBAf(0.47f, 0.75f, 0.34f, 1.00f)}));

/* Components */

// Buttons

// Knobs
Knob::Knob(const Size size) {
	auto box_size = detail::toVec(static_cast<detail::Size>(size));
	std::string prefix = COMPONENT_RESOURCE_DIR + "Knob_" + detail::toString(static_cast<detail::Size>(size)) + "_";

	setBackdropImage(rack::asset::plugin(pluginInstance, prefix + RESOURCE_SUFFIX), box_size);
	setIndicatorImage(rack::asset::plugin(pluginInstance, prefix + "Indicator_Grunge_" + RESOURCE_SUFFIX), box_size);
}

// Switches
SlideSwitch::SlideSwitch(const Orientation orientation, const Positions positions) {
	auto size = rack::window::mm2px(orientation == Orientation::HORIZONTAL ? rack::math::Vec(8.0f, 4.0f)
																		   : rack::math::Vec(4.0f, 8.0f));
	std::string prefix = COMPONENT_RESOURCE_DIR + "Slide_Switch_" +
						 detail::toString(static_cast<detail::Orientation>(orientation)) + "_";

	if (orientation == Orientation::HORIZONTAL) {
		std::string left = rack::asset::plugin(pluginInstance, prefix + "Left_" + RESOURCE_SUFFIX);
		std::string right = rack::asset::plugin(pluginInstance, prefix + "Right_" + RESOURCE_SUFFIX);

		if (positions == Positions::DOUBLE_THROW) {
			setFrames({left, right}, size);

		} else if (positions == Positions::TRIPLE_THROW) {
			std::string center = rack::asset::plugin(pluginInstance, prefix + "Center_" + RESOURCE_SUFFIX);
			setFrames({left, center, right}, size);
		}

	} else {
		std::string bottom = rack::asset::plugin(pluginInstance, prefix + "Bottom_" + RESOURCE_SUFFIX);
		std::string top = rack::asset::plugin(pluginInstance, prefix + "Top_" + RESOURCE_SUFFIX);

		if (positions == Positions::DOUBLE_THROW) {
			setFrames({bottom, top}, size);

		} else if (positions == Positions::TRIPLE_THROW) {
			std::string center = rack::asset::plugin(pluginInstance, prefix + "Center_" + RESOURCE_SUFFIX);
			setFrames({bottom, center, top}, size);
		}
	}
}

// Ports
Port::Port() {
	auto size = rack::window::mm2px(rack::math::Vec(8.5f, 8.5f));

	setImage(rack::asset::plugin(pluginInstance, COMPONENT_RESOURCE_DIR + "Port_" + RESOURCE_SUFFIX), size);
	setCustomColors(CABLE_COLORS);
}

// Test build warning
TestBuildWarning::TestBuildWarning() : component::TextLabel("TEST BUILD:\nDO NOT DISTRIBUTE") {
	setSize(8.0f);
	setColor(1.0f, 1.0f, 1.0f);
}

void TestBuildWarning::attach(rack::app::ModuleWidget* widget) {
	setPositionPx(widget->box.size.x * 0.5f, rack::app::RACK_GRID_WIDTH * 1.65f);
	center();
	component::TextLabel::attach(widget);
}

}  // namespace numberstation
}  // namespace skin
}  // namespace slime
