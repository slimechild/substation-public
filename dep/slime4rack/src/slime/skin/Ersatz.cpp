/**********
 * Copyright (c) 2022, Coriander Violet Pines. All rights reserved.
 * This file is part of slime4rack and is governed by the attached license.
 *
 * Author: C. V. Pines
 */

#include <slime/skin/Ersatz.hpp>

#include <slime/Assert.hpp>
#include <slime/Color.hpp>

#include <memory>
#include <string>
#include <utility>

#include <nanovg.h>
#include <app/common.hpp>
#include <asset.hpp>
#include <helpers.hpp>
#include <plugin/Plugin.hpp>
#include <window/Svg.hpp>

extern rack::plugin::Plugin* pluginInstance;

namespace slime {
namespace skin {
namespace ersatz {

/* Skin constants */
const std::string RESOURCE_SUFFIX = "4x.png";
const std::string COMPONENT_RESOURCE_DIR = "res/slime4rack/skin/ersatz/";

/* Components */

// Buttons

// Knobs
Knob::Knob(const Size size) {
	auto box_size = detail::toVec(static_cast<detail::Size>(size));
	std::string prefix = COMPONENT_RESOURCE_DIR + "Knob_" + detail::toString(static_cast<detail::Size>(size)) + "_";

	setBackdropImage("", box_size);
	setIndicatorImage(rack::asset::plugin(pluginInstance, prefix + "Indicator_" + RESOURCE_SUFFIX), box_size);
}

// Switches
SlideSwitch::SlideSwitch(const Orientation orientation, const Positions positions) {
	auto size = rack::window::mm2px(orientation == Orientation::HORIZONTAL ? rack::math::Vec(8.0f, 4.0f)
																		   : rack::math::Vec(4.0f, 8.0f));
	std::string prefix = COMPONENT_RESOURCE_DIR + "Slide_Switch_" +
						 detail::toString(static_cast<detail::Orientation>(orientation)) + "_";

	if (orientation == Orientation::HORIZONTAL) {
		std::string left = rack::asset::plugin(pluginInstance, prefix + "Left_" + RESOURCE_SUFFIX);
		std::string right = rack::asset::plugin(pluginInstance, prefix + "Right_" + RESOURCE_SUFFIX);

		if (positions == Positions::DOUBLE_THROW) {
			setFrames({left, right}, size);

		} else if (positions == Positions::TRIPLE_THROW) {
			std::string center = rack::asset::plugin(pluginInstance, prefix + "Center_" + RESOURCE_SUFFIX);
			setFrames({left, center, right}, size);
		}

	} else {
		std::string bottom = rack::asset::plugin(pluginInstance, prefix + "Bottom_" + RESOURCE_SUFFIX);
		std::string top = rack::asset::plugin(pluginInstance, prefix + "Top_" + RESOURCE_SUFFIX);

		if (positions == Positions::DOUBLE_THROW) {
			setFrames({bottom, top}, size);

		} else if (positions == Positions::TRIPLE_THROW) {
			std::string center = rack::asset::plugin(pluginInstance, prefix + "Center_" + RESOURCE_SUFFIX);
			setFrames({bottom, center, top}, size);
		}
	}
}

// Ports
Port::Port() {
	auto size = rack::window::mm2px(rack::math::Vec(8.5f, 8.5f));

	setImage(rack::asset::plugin(pluginInstance, COMPONENT_RESOURCE_DIR + "Port_" + RESOURCE_SUFFIX), size);
}

// Test build warning
TestBuildWarning::TestBuildWarning() : component::TextLabel("TEST BUILD:\nDO NOT DISTRIBUTE") {
	setSize(8.0f);
	setColor(1.0f, 1.0f, 1.0f);
}

void TestBuildWarning::attach(rack::app::ModuleWidget* widget) {
	setPositionPx(widget->box.size.x * 0.5f, rack::app::RACK_GRID_WIDTH * 1.65f);
	center();
	component::TextLabel::attach(widget);
}

// Lights
MonochromeLED::MonochromeLED(const Size size, const Color light_color) {
	auto box_size = detail::toVec(static_cast<detail::Size>(size));
	std::string prefix = COMPONENT_RESOURCE_DIR + "LED_" + detail::toString(static_cast<detail::Size>(size)) + "_" +
						 detail::toString(static_cast<detail::Color>(light_color)) + "_";

	setBackdropImage(rack::asset::plugin(pluginInstance, prefix + RESOURCE_SUFFIX), box_size);
	setHaloImage(rack::asset::plugin(pluginInstance, prefix + "Light_" + RESOURCE_SUFFIX), box_size * 3.0f);
}

// Button
Button::Button(const Size size, const OffColor off_color, const OnColor on_color) {
	auto box_size = detail::toVec(static_cast<detail::Size>(size));
	std::string prefix = COMPONENT_RESOURCE_DIR + "Button_" + detail::toString(static_cast<detail::Size>(size)) + "_";
	std::string off = detail::toString(static_cast<detail::Color>(off_color)) + "_";
	std::string on = detail::toString(static_cast<detail::Color>(on_color)) + "_";

	setFrames({rack::asset::plugin(pluginInstance, prefix + off + RESOURCE_SUFFIX),
			   rack::asset::plugin(pluginInstance, prefix + on + RESOURCE_SUFFIX)},
			  box_size);
}

// Icons
BrandLogo::BrandLogo(const Size size) : component::SvgIcon(getPath(size)) {}

void BrandLogo::attach(rack::app::ModuleWidget* widget) {
	setPositionPx(widget->box.size.x * 0.5f, rack::app::RACK_GRID_HEIGHT - rack::app::RACK_GRID_WIDTH * 1.05f);
	center();
	SvgIcon::attach(widget);
}

std::string BrandLogo::getPath(const Size size) {
	return rack::asset::plugin(
		pluginInstance, COMPONENT_RESOURCE_DIR + "Logo_" + detail::toString(static_cast<detail::Size>(size)) + ".svg");
}

}  // namespace ersatz
}  // namespace skin
}  // namespace slime
