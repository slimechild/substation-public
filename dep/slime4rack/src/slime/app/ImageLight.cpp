/**********
 * Copyright (c) 2022, Coriander Violet Pines. All rights reserved.
 * This file is part of slime4rack and is governed by the attached license.
 *
 * Author: C. V. Pines
 */

#include <slime/app/ImageLight.hpp>

namespace slime {
namespace app {

ImageLight::ImageLight() {
	_backgroundWidget = new widget::ImageWidget;
	addChild(_backgroundWidget);

	_glowWidget = new widget::ImageWidget;
	_glowWidget->setBlendMode(widget::ImageWidget::BlendMode::SCREEN);
	_glowWidget->useDrawLayerMethod(_GLOW_LAYER);
	addChild(_glowWidget);
}

void ImageLight::setBackdropImage(const std::string& path, const rack::math::Vec& size) {
	_backgroundWidget->setImage(path);
	_backgroundWidget->box.size = size;

	box.size = size;
}

void ImageLight::setHaloImage(const std::string& path, const rack::math::Vec& size) {
	_glowWidget->setImage(path);
	_glowWidget->box.size = size;
	_glowWidget->box.pos =
		_backgroundWidget->box.pos.plus(_backgroundWidget->box.size.minus(_glowWidget->box.size).mult(0.5f));
}

void ImageLight::drawLayer(const DrawArgs& args, int layer) {
	if (layer == _GLOW_LAYER) {
		if (module) {
			float brightness = module->lights[firstLightId].getBrightness();
			_glowWidget->setAlpha(brightness);
		} else {
			_glowWidget->setAlpha(0.0f);
		}
	}

	rack::widget::Widget::drawLayer(args, layer);
}

}  // namespace app
}  // namespace slime
