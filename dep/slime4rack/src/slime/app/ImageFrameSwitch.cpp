/**********
 * Copyright (c) 2022, Coriander Violet Pines. All rights reserved.
 * This file is part of slime4rack and is governed by the attached license.
 *
 * Author: C. V. Pines
 */

#include <slime/app/ImageFrameSwitch.hpp>

namespace slime {
namespace app {

ImageFrameSwitch::ImageFrameSwitch() {
	_fb = new rack::widget::FramebufferWidget;
	addChild(_fb);

	_iw = new widget::ImageWidget;
	addChild(_iw);

	// Default to 0-frame when unspecified (e.g. screenshots, browser)
	_index = 0;
}

void ImageFrameSwitch::setFrames(const FrameSet& frames, const rack::math::Vec& size) {
	_frames = frames;

	_iw->setImage(_frames[_index]);

	box.size = size;
	_iw->box.size = size;
	_fb->box.size = size;
	_fb->dirty = true;
}  // namespace app

void ImageFrameSwitch::onChange(const rack::event::Change& e) {
	rack::engine::ParamQuantity* paramQuantity = getParamQuantity();
	if (!_frames.empty() && paramQuantity) {
		_index = static_cast<std::size_t>(std::round(paramQuantity->getValue() - paramQuantity->getMinValue()));
		_index = rack::math::clamp(_index, 0, static_cast<std::size_t>(_frames.size() - 1));
		_iw->setImage(_frames[_index]);
		_fb->dirty = true;
	}

	Switch::onChange(e);
}

}  // namespace app
}  // namespace slime
