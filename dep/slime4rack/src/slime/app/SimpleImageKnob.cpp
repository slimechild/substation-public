/**********
 * Copyright (c) 2022, Coriander Violet Pines. All rights reserved.
 * This file is part of slime4rack and is governed by the attached license.
 *
 * Author: C. V. Pines
 */

#include <slime/IntellisenseFix.hpp>
#include <slime/app/SimpleImageKnob.hpp>

#include <math.hpp>

namespace slime {
namespace app {

SimpleImageKnob::SimpleImageKnob() {
	_fb = new rack::widget::FramebufferWidget;
	addChild(_fb);

	_backgroundWidget = new widget::ImageWidget;
	_fb->addChild(_backgroundWidget);

	_indicatorTransform = new rack::widget::TransformWidget;
	_fb->addChild(_indicatorTransform);

	_indicatorWidget = new widget::ImageWidget;
	_indicatorTransform->addChild(_indicatorWidget);
}

void SimpleImageKnob::setBackdropImage(const std::string& path, const rack::math::Vec& size) {
	_backgroundWidget->setImage(path);
	_backgroundWidget->box.size = size;

	_fb->box = _backgroundWidget->box;
	_fb->dirty = true;

	box.size = _backgroundWidget->box.size;
}

void SimpleImageKnob::setIndicatorImage(const std::string& path, const rack::math::Vec& size) {
	_indicatorWidget->setImage(path);
	_indicatorWidget->box.size = size;

	_indicatorTransform->box.size = _indicatorWidget->box.size;

	_fb->box = _fb->box.expand(_indicatorWidget->box);
	_fb->dirty = true;
}

void SimpleImageKnob::onChange(const rack::event::Change& e) {
	rack::engine::ParamQuantity* paramQuantity = getParamQuantity();
	if (paramQuantity) {
		float value = paramQuantity->getSmoothValue();
		float angle;
		if (!paramQuantity->isBounded()) {
			// Number of rotations equals value for unbounded range
			angle = value * (2 * M_PI);
		} else if (paramQuantity->getRange() == 0.0f) {
			// Center angle for zero range
			angle = (minAngle + maxAngle) / 2.f;
		} else {
			// Proportional angle for finite range
			angle = rack::math::rescale(value, paramQuantity->getMinValue(), paramQuantity->getMaxValue(), minAngle,
										maxAngle);
		}

		angle = std::fmod(angle, 2 * M_PI);
		_indicatorTransform->identity();
		rack::math::Vec center = _indicatorWidget->box.getCenter();
		_indicatorTransform->translate(center);
		_indicatorTransform->rotate(angle);
		_indicatorTransform->translate(center.neg());
		_fb->dirty = true;
	}

	Knob::onChange(e);
}

}  // namespace app
}  // namespace slime
