/**********
 * Copyright (c) 2022, Coriander Violet Pines. All rights reserved.
 * This file is part of slime4rack and is governed by the attached license.
 *
 * Author: C. V. Pines
 */

#include <slime/app/ModulePanel.hpp>

#include <app/common.hpp>
#include <context.hpp>
#include <math.hpp>
#include <widget/SvgWidget.hpp>
#include <window/Window.hpp>

namespace slime {
namespace app {

ModulePanel::ModulePanel(std::size_t width) {
	_width = width;
	box.size =
		rack::math::Vec(0.5f + static_cast<float>(_width) * rack::app::RACK_GRID_WIDTH, rack::app::RACK_GRID_HEIGHT);
}

void ModulePanel::step() {
	// From rack/app/SvgPanel.cpp
	if (APP->window->pixelRatio < 2.0) {
		oversample = 2.0;
	}

	Widget::step();
}

ModulePanel* ModulePanel::addSolidBackground(const NVGcolor& color) {
	slime::widget::SolidRectangleWidget* solid = new slime::widget::SolidRectangleWidget;

	solid->setColor(color);
	solid->box.size = box.size;
	addChild(solid);

	return this;
}

ModulePanel* ModulePanel::addImageBackground(const std::string& path) {
	slime::widget::ImageWidget* image = new slime::widget::ImageWidget;

	image->setImage(path);
	image->box.size = box.size;
	addChild(image);

	return this;
}

ModulePanel* ModulePanel::addSvgBackground(const std::string& path) {
	rack::widget::SvgWidget* svg = new rack::widget::SvgWidget;

	svg->setSvg(rack::window::Svg::load(path));
	addChild(svg);

	return this;
}

ModulePanel* ModulePanel::addBorder(float r, float g, float b, float a) {
	slime::widget::BorderWidget* border = new slime::widget::BorderWidget;

	border->setColor(r, g, b, a);
	border->box.size = box.size;
	addChild(border);

	return this;
}

ModulePanel* ModulePanel::attach(rack::app::ModuleWidget* module_widget) {
	module_widget->setPanel(this);
	return this;
}

}  // namespace app
}  // namespace slime
