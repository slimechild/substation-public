/**********
 * Copyright (c) 2022, Coriander Violet Pines. All rights reserved.
 * This file is part of slime4rack and is governed by the attached license.
 *
 * Author: C. V. Pines
 */

#include <slime/app/ImagePort.hpp>

#include <algorithm>

namespace slime {
namespace app {

ImagePort::ImagePort() {
	_fb = new rack::widget::FramebufferWidget;
	addChild(_fb);

	_iw = new widget::ImageWidget;
	_fb->addChild(_iw);
}

void ImagePort::setImage(const std::string& path, const rack::math::Vec& size) {
	_iw->setImage(path);
	_iw->box.size = size;
	_fb->box.size = _iw->box.size;
	box.size = _iw->box.size;

	_fb->dirty = true;
}

}  // namespace app
}  // namespace slime
