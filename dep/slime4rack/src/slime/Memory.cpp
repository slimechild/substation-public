/**********
 * Copyright (c) 2022, Coriander Violet Pines. All rights reserved.
 * This file is part of slime4rack and is governed by the attached license.
 * 
 * Author: C. V. Pines
 */

#if defined(SLIME_MEMORY)

#include <cstdlib>
#include <new>

void* operator new(std::size_t n) {
	void* ptr = std::malloc(n);
	if (!ptr) {
		throw std::bad_alloc{};
	}

	return ptr;
}

void operator delete(void* p) throw() {
	std::free(p);
}

#endif