/**********
 * Copyright (c) 2022, Coriander Violet Pines. All rights reserved.
 * This file is part of slime4rack and is governed by the attached license.
 *
 * Author: C. V. Pines
 */

#include <slime/engine/ParamChangeHelper.hpp>

namespace slime {
namespace engine {

ParamChangeHelper::ParamChangeHelper(rack::engine::Module* module) : _module(module){};

void ParamChangeHelper::reset() {
	_values.clear();
}

bool ParamChangeHelper::changed(size_t id) {
	bool res = false;

	float val = _module->params[id].getValue();
	auto found = _values.find(id);
	if (found == _values.end() || found->second != val) {
		_values[id] = val;
		res = true;
	}

	return res;
}

float ParamChangeHelper::value(std::size_t id) {
	return _values[id];
}

}  // namespace engine
}  // namespace slime