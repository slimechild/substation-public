/**********
 * Copyright (c) 2022, Coriander Violet Pines. All rights reserved.
 * This file is part of slime4rack and is governed by the attached license.
 *
 * Author: C. V. Pines
 */

#include <slime/engine/History.hpp>

#include <context.hpp>

namespace slime {
namespace engine {

ModuleHistoryGuard::ModuleHistoryGuard(rack::engine::Module* module) {
	_module = module;
	_history = new rack::history::ModuleChange;
	_history->moduleId = _module->id;

	// Serialize pre-state
	_history->oldModuleJ = _module->toJson();
}
ModuleHistoryGuard::ModuleHistoryGuard(rack::engine::Module* module, const std::string& name)
	: ModuleHistoryGuard(module) {
	_history->name = name;
}

ModuleHistoryGuard::~ModuleHistoryGuard() {
	// Serialize post-state
	_history->newModuleJ = _module->toJson();

	// Transfer ownership to application context
	rack::contextGet()->history->push(_history);
}

}  // namespace engine
}  // namespace slime
