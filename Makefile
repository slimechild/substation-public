RACK_DIR ?= ../../Rack-SDK

# Compiler flags
FLAGS += 
CFLAGS +=
CXXFLAGS += -Werror

# Linker flags
LDFLAGS += 

# Dependencies
DEPS += slime4rack
FLAGS += -Idep/slime4rack/include
SOURCES += $(wildcard dep/slime4rack/src/slime/*.cpp)
SOURCES += $(wildcard dep/slime4rack/src/slime/**/*.cpp)
slime4rack:
	rm -rf res/slime4rack/*
	mkdir res/slime4rack
	cp -r dep/slime4rack/res/* res/slime4rack

# Plugin sources
SOURCES += $(wildcard src/*.cpp)

# Distributables
DISTRIBUTABLES += res
DISTRIBUTABLES += LICENSE.md


test: CXXFLAGS += -DSLIME_DEBUG
test: install

include $(RACK_DIR)/plugin.mk
